/*
 * GameAction.h
 *
 *  Created on: 2009-11-30
 *      Author: Revers
 */

#ifndef GAMEACTION_H_
#define GAMEACTION_H_

#include <string>

using std::string;

class GameAction {
private:
	enum KeyState {
		STATE_RELEASED, STATE_PRESSED, STATE_WAITING_FOR_RELEASE
	};

	string name;
	int behavior;
	int amount;
	KeyState state;
public:

	static const int NORMAL = 0;
	static const int DETECT_INITAL_PRESS_ONLY = 1;
	GameAction(string name, int behavior = NORMAL) :
		name(name), behavior(behavior) {
		reset();
	}

	string getName() {
		return name;
	}

	void reset() {
		state = STATE_RELEASED;
		amount = 0;
	}

	void tap() {
		press();
		release();
	}

	void press() {
		press(1);
	}

	void press(int amount);

	void release() {
		state = STATE_RELEASED;
	}

	bool isPressed() {
		return (getAmount() != 0);
	}

	int getAmount();

	virtual ~GameAction() {
	}
};

#endif /* GAMEACTION_H_ */
