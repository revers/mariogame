/*
 * ResourceManager.cpp
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#include <fstream>
#include <sstream>
#include <cstdlib>
#include "ResourceManager.h"
#include "TileMapRenderer.h"
#include "EvilMashroom.h"
#include "Player.h"
#include "Fly.h"
#include "Goodie.h"
#include "QuestionMarkTile.h"
#include "BricksTile.h"
#include "Fireball.h"
#include "PipeEntranceTile.h"
#include "Plant.h"
#include "Turtle.h"

#include <iostream>
using namespace std;

/*
 * TODO: Dodac usuwanie animacji i sprite'y-hosty
 */

ResourceManager::ResourceManager() {
	playerSprite = NULL;
	hostSprites = NULL;
	hostTiles = NULL;
	hostAdditionalSprites = NULL;
	currentMap = 0;

	//cout << "ResourceManger constructor1" << endl;
	loadPlayerSprite();
	loadHostAdditionalSprites();
	loadBrokenBricks();
	//cout << "ResourceManger constructor2" << endl;
	heartImage = loadImage("images/heart.png");
}

ResourceManager::~ResourceManager() {
	//delete tiles;
	destroyHosts();
	destroyPlayer();

	if (hostAdditionalSprites != NULL) {
		for (AdditionalSpritesMapIter iter = hostAdditionalSprites->begin(); iter
				!= hostAdditionalSprites->end(); iter++) {
			iter->second->cleanHost();
			delete iter->second;
		}
		hostAdditionalSprites->clear();
	}

	brokenBricks->cleanHost();
	delete brokenBricks;
	SDL_FreeSurface(heartImage);
}

SDL_Surface* ResourceManager::loadImage(std::string filename) {
	//filename = std::string("images/").append(filename);
	SDL_Surface* loadedImage = NULL;
	SDL_Surface* optimizedImage = NULL;

	loadedImage = IMG_Load(filename.c_str());
	if (loadedImage != NULL) {
		optimizedImage = SDL_DisplayFormat(loadedImage);
		SDL_FreeSurface(loadedImage);

		if (optimizedImage != NULL) {
			Uint32 colorkey = SDL_MapRGB(optimizedImage->format, IGNORED_RED,
					IGNORED_GREEN, IGNORED_BLUE);
			SDL_SetColorKey(optimizedImage, SDL_SRCCOLORKEY, colorkey);
		} else {
			return NULL;
		}
	} else {
		return NULL;
	}

	return optimizedImage;
}

Uint32 ResourceManager::getPixel(SDL_Surface *surface, int x, int y) {
	Uint32 *pixels = (Uint32 *) surface->pixels;

	return pixels[(y * surface->w) + x];
}

void ResourceManager::putPixel(SDL_Surface *surface, int x, int y, Uint32 pixel) {
	Uint32 *pixels = (Uint32 *) surface->pixels;
	pixels[(y * surface->w) + x] = pixel;
}

SDL_Surface* ResourceManager::flipSurface(SDL_Surface *surface, int flags) {
	SDL_Surface *flipped = NULL;

	//If the image is color keyed
	if (surface->flags & SDL_SRCCOLORKEY) {
		flipped = SDL_CreateRGBSurface(SDL_SWSURFACE, surface->w, surface->h,
				surface->format->BitsPerPixel, surface->format->Rmask,
				surface->format->Gmask, surface->format->Bmask, 0);
	}
	//Otherwise
	else {
		flipped = SDL_CreateRGBSurface(SDL_SWSURFACE, surface->w, surface->h,
				surface->format->BitsPerPixel, surface->format->Rmask,
				surface->format->Gmask, surface->format->Bmask,
				surface->format->Amask);
	}

	//If the surface must be locked
	if (SDL_MUSTLOCK(surface)) {
		//Lock the surface
		SDL_LockSurface(surface);
	}

	//Go through columns
	for (int x = 0, rx = flipped->w - 1; x < flipped->w; x++, rx--) {
		//Go through rows
		for (int y = 0, ry = flipped->h - 1; y < flipped->h; y++, ry--) {
			//Get pixel
			Uint32 pixel = getPixel(surface, x, y);

			//Copy pixel
			if ((flags & FLIP_VERTICAL) && (flags & FLIP_HORIZONTAL)) {
				putPixel(flipped, rx, ry, pixel);
			} else if (flags & FLIP_HORIZONTAL) {
				putPixel(flipped, rx, y, pixel);
			} else if (flags & FLIP_VERTICAL) {
				putPixel(flipped, x, ry, pixel);
			}
		}
	}

	//Unlock surface
	if (SDL_MUSTLOCK(surface)) {
		SDL_UnlockSurface(surface);
	}

	//Copy color key
	if (surface->flags & SDL_SRCCOLORKEY) {
		SDL_SetColorKey(flipped, SDL_RLEACCEL | SDL_SRCCOLORKEY,
				surface->format->colorkey);
	}

	//Return flipped surface
	return flipped;
}

TileMap* ResourceManager::loadNextMap() {
	destroyHosts();
	TileMap *map = NULL;
	while (map == NULL) {
		currentMap++;
		std::ostringstream sout;
		sout << "maps/map" << currentMap << ".txt";
		//cout << "blebl loadNextMap()" << endl;
		map = loadMap(sout.str());
		//	cout << "blebl loadNext;)()" << endl;
		if (map == NULL) {
			if (currentMap == 1) {
				// no maps to load!
				return NULL;
			}
			currentMap = 0;
			map = NULL;
		}
	}

	return map;
}

TileMap* ResourceManager::reloadMap() {
	std::ostringstream sout;
	sout << "maps/map" << currentMap << ".txt";
	return loadMap(sout.str());
}

TileMap* ResourceManager::loadMap(std::string filename) {
	std::ifstream in(filename.c_str());
	cout << "LOAD MAP" << endl;

	if (!in) {
		return NULL;
	}
	cout << "LOAD MAP2" << endl;

	char* tagBegins[] = { "<background>", "<tiles>", "<enemies>", "<goodies>",
			"<map>", "<entrance>", "<nextlevel>" };
	char* tagEnds[] = { "</background>", "</tiles>", "</enemies>",
			"</goodies>", "</map>", "</entrance>", "</nextlevel>" };
	const int TAGS = sizeof(tagBegins) / sizeof(char*);

	const int ENTRY_ARRAY_SIZE = 7;
	const int NEXT_LEVEL_ARRAY_SIZE = 3;
	string entrArr[ENTRY_ARRAY_SIZE];
	int entrArrIndex = 0;
	string nextLvlArr[NEXT_LEVEL_ARRAY_SIZE];
	int nextLvlArrIndex = 0;
	PipeEntryList *pipeEntryList = new PipeEntryList;

	string background = "";
	string str = "";
	std::vector<std::string> mapLines;
	bool inTag = false;
	int index = 0;
	while (in.eof() == false) {
		getline(in, str);
		if (str.length() == 0 && str[0] == '#')
			continue;
		if (inTag == false) {
			for (int i = 0; i < TAGS; i++) {
				if (str == tagBegins[i]) {
					inTag = true;
					index = i;
					cout << endl;
					break;
				}
			}
		} else {
			if (str == tagEnds[index]) {
				inTag = false;
				continue;
			}
			switch (index) {
			case 0:
				background = str;
				break;
			case 1:
				loadHostTile(str[0]);
				break;
			case 2:
				loadHostEnemy(str[0]);
				break;
			case 3:
				loadHostGoodie(str[0]);
				break;
			case 4:
				mapLines.push_back(str);
				break;

			case 5:
				entrArr[entrArrIndex++] = str;
				if (entrArrIndex == ENTRY_ARRAY_SIZE) {
					pipeEntryList->push_back(getPipeEntry(entrArr));
					entrArrIndex = 0;
				}
				break;
			case 6:
				nextLvlArr[nextLvlArrIndex++] = str;
				break;

			}

		}
	}
	in.close();
	cout << "LOAD MAP#2" << endl;

	int width = mapLines[0].length();
	int height = mapLines.size();
	cout << "width = " << width << "; height = " << height << endl;

	TileMap *newMap = new TileMap(width, height);
	if (pipeEntryList->size() != 0)
		newMap->setPipeEntryList(pipeEntryList);
	if (nextLvlArrIndex != 0)
		newMap->setNextLevelEntry(getNextLevelEntry(nextLvlArr));

	for (int y = 0; y < height; y++) {

		for (int x = 0; x < static_cast<int> (mapLines[y].length()); x++) {
			char ch = (mapLines[y])[x];

			if (addTile(newMap, x, y, ch) == false)
				addSprite(newMap, x, y, ch);
		}
	}

	if (background.length() != 0) {
		newMap->setBackground(loadImage(background));
		//newMap->
		// TODO: dodac tlo
	}

	cout << "LOAD MAP#3" << endl;
	Player *player = (Player*) playerSprite->clone();
	player->setX(TileMapRenderer::tilesToPixels(3));
	cout << "loadMapEnd3" << endl;
	player->setY(0);
	cout << "loadMapEnd4" << endl;
	newMap->setPlayer(player);
	cout << "LOAD MAP#4" << endl;
	return newMap;
}

PipeEntry* ResourceManager::getPipeEntry(string *entrArr) {
	PipeEntry* pipeEntry = new PipeEntry();
	pipeEntry->setSubmapName(new string(entrArr[0]));
	pipeEntry->setEntranceSide((entrArr[1] == "V" ? BOTTOM : RIGHT));
	stringstream ss;
	ss << entrArr[2] << endl;
	int i;
	ss >> i;
	pipeEntry->setEntranceX(i);
	ss << entrArr[3] << endl;
	ss >> i;
	pipeEntry->setEntranceY(i);
	pipeEntry->setExitSide((entrArr[4] == "V" ? BOTTOM : RIGHT));
	ss << entrArr[5] << endl;
	ss >> i;
	pipeEntry->setExitX(i);
	ss << entrArr[6] << endl;
	ss >> i;
	pipeEntry->setExitY(i);
	return pipeEntry;
}

NextLevelEntry* ResourceManager::getNextLevelEntry(string *nextLvlArr) {
	NextLevelEntry* entry = new NextLevelEntry;
	stringstream ss;
	ss << nextLvlArr[1] << endl;
	int i;
	ss >> i;
	entry->setX(i);
	ss << nextLvlArr[2] << endl;
	ss >> i;
	entry->setY(i);
	entry->setSide((nextLvlArr[0] == "V" ? BOTTOM : RIGHT));
	return entry;
}

void ResourceManager::loadHostTile(char c) {
	//cout << "In loadHostTile" << endl;
	if (hostTiles == NULL)
		hostTiles = new TilesMap;
	else {
		TilesMapIter iter = hostTiles->find(c);
		if (iter != hostTiles->end())
			return;
	}
	switch (c) {
	case '@':
		(*hostTiles)[c] = new BricksTile(loadImage("images/map/cegly.png"));
		break;
		//	case 'T':
		//		(*hostTiles)[c] = new Tile(loadImage("images/map/kanal.png"));
		break;
	case '!':
		(*hostTiles)[c] = new Tile(loadImage("images/map/trawa.png"));
		break;
	case 'Z':
		(*hostTiles)[c] = new Tile(loadImage("images/map/ziemia.png"));
		break;
	case '/':
		(*hostTiles)[c] = new PipeEntranceTile(loadImage(
				"images/map/kanal1x1.png"), LEFT);
		break;
	case '\\':
		(*hostTiles)[c] = new PipeEntranceTile(loadImage(
				"images/map/kanal1x2.png"), RIGHT);
		break;
	case '<':
		(*hostTiles)[c] = new Tile(loadImage("images/map/kanal2x1.png"));
		break;
	case '>':
		(*hostTiles)[c] = new Tile(loadImage("images/map/kanal2x2.png"));
		break;
	case 'a':
		(*hostTiles)[c] = new PipeEntranceTile(loadImage(
				"images/map/vkanal1x1.png"), TOP);
		break;
	case 'b':
		(*hostTiles)[c] = new PipeEntranceTile(loadImage(
				"images/map/vkanal1x2.png"), BOTTOM);
		break;
	case 'c':
		(*hostTiles)[c] = new Tile(loadImage("images/map/vkanal2x1.png"));
		break;
	case 'd':
		(*hostTiles)[c] = new Tile(loadImage("images/map/vkanal2x2.png"));
		break;
	case 'g':
		(*hostTiles)[c] = new Tile(loadImage("images/map/gcegly.png"));
		break;
	case 'h':
		(*hostTiles)[c] = new Tile(loadImage("images/map/gground.png"));
		break;
	case 'e': {
		Animation *anim = new Animation();
		anim->addFrame(loadImage("images/map/znak_zap.png"), 400);
		anim->addFrame(loadImage("images/map/znak_zap2.png"), 400);
		SDL_Surface* emptyBox = loadImage("images/map/znak_zap_pusty.png");
		(*hostTiles)[c] = new QuestionMarkTile(anim, emptyBox,
				ResourceManager::SPRITE_STAR, 10);
		break;
	}
	case 'f': {
		Animation *anim = new Animation();
		anim->addFrame(loadImage("images/map/znak_zap.png"), 400);
		anim->addFrame(loadImage("images/map/znak_zap2.png"), 400);
		SDL_Surface* emptyBox = loadImage("images/map/znak_zap_pusty.png");
		(*hostTiles)[c] = new QuestionMarkTile(anim, emptyBox,
				ResourceManager::SPRITE_MASHROOM, 1);
		break;
	}

	case 'A':
		(*hostTiles)[c] = new PipeEntranceTile(loadImage(
				"images/map/red_kanal1x1.png"), LEFT);
		break;
	case 'B':
		(*hostTiles)[c] = new PipeEntranceTile(loadImage(
				"images/map/red_kanal1x2.png"), RIGHT);
		break;
	case 'C':
		(*hostTiles)[c] = new Tile(loadImage("images/map/red_kanal2x1.png"));
		break;
	case 'D':
		(*hostTiles)[c] = new Tile(loadImage("images/map/red_kanal2x2.png"));
		break;
	case 'E':
		(*hostTiles)[c] = new PipeEntranceTile(loadImage(
				"images/map/red_vkanal1x1.png"), TOP);
		break;
	case 'F':
		(*hostTiles)[c] = new PipeEntranceTile(loadImage(
				"images/map/red_vkanal1x2.png"), BOTTOM);
		break;
	case 'G':
		(*hostTiles)[c] = new Tile(loadImage("images/map/red_vkanal2x1.png"));
		break;
	case 'H':
		(*hostTiles)[c] = new Tile(loadImage("images/map/red_vkanal2x2.png"));
		break;
	}

}
void ResourceManager::loadHostGoodie(char c) {
	if (hostSprites == NULL)
		hostSprites = new SpritesMap;
	else {
		SpritesMapIter iter = hostSprites->find(c);
		if (iter != hostSprites->end())
			return;
	}
	switch (c) {
	case 'o': {
		Animation *anim = new Animation();
		anim->addFrame(loadImage("images/goodies/star1.png"), 100);
		anim->addFrame(loadImage("images/goodies/star2.png"), 100);
		anim->addFrame(loadImage("images/goodies/star3.png"), 100);
		anim->addFrame(loadImage("images/goodies/star4.png"), 100);
		(*hostSprites)[c] = new Star(anim);
		break;
	}
	}
}
void ResourceManager::loadHostEnemy(char c) {
	if (hostSprites == NULL)
		hostSprites = new SpritesMap;
	else {
		SpritesMapIter iter = hostSprites->find(c);
		if (iter != hostSprites->end())
			return;
	}

	switch (c) {
	case '1': {
		const int w = 2;
		const int h = 2;
		SDL_Surface* images[w][h];

		images[0][0] = loadImage("images/enemies/shroom1.png");
		images[0][1] = loadImage("images/enemies/shroom2.png");

		for (int i = 0; i < h; i++) {
			images[1][i] = getMirrorImage(images[0][i]);
		}
		const int length = 2;
		Animation* grubAnim[length];
		for (int i = 0; i < length; i++) {
			grubAnim[i] = createMashroomAnim(images[i][0], images[i][1]);
		}

		SDL_Surface* deadImage = loadImage("images/enemies/shroom_dead.png");
		(*hostSprites)[c] = new EvilMashroom(grubAnim[0], grubAnim[1],
				deadImage);
		break;
	}
	case '2': {
		const int w = 2;
		const int h = 3;
		SDL_Surface* images[w][h];

		images[0][0] = loadImage("images/enemies/fly1.png");
		images[0][1] = loadImage("images/enemies/fly2.png");
		images[0][2] = loadImage("images/enemies/fly3.png");

		for (int i = 0; i < h; i++) {
			images[1][i] = getMirrorImage(images[0][i]);
		}
		const int length = 2;
		Animation* flyAnim[length];
		for (int i = 0; i < length; i++) {
			flyAnim[i]
					= createFlyAnim(images[i][0], images[i][1], images[i][2]);
		}

		SDL_Surface* deadImage = loadImage("images/enemies/fly1.png");

		(*hostSprites)[c] = new Fly(flyAnim[0], flyAnim[1], deadImage);
		break;
	}
	case '3': {
		Animation *plantAnim = new Animation;
		plantAnim->addFrame(loadImage("images/enemies/plant1.png"), 220);
		plantAnim->addFrame(loadImage("images/enemies/plant2.png"), 220);
		(*hostSprites)[c] = new Plant(plantAnim);
		break;
	}

	case '4': {
		Animation *turtleAnim1 = new Animation;
		SDL_Surface* t1 = loadImage("images/enemies/turtle1.png");
		SDL_Surface* t2 = loadImage("images/enemies/turtle2.png");
		turtleAnim1->addFrame(t1, 400);
		turtleAnim1->addFrame(t2, 400);

		Animation *turtleAnim2 = new Animation;
		turtleAnim2->addFrame(getMirrorImage(t1), 400);
		turtleAnim2->addFrame(getMirrorImage(t2), 400);

		SDL_Surface* s1 = loadImage("images/enemies/turtle_shell2.png");
		SDL_Surface* s2 = loadImage("images/enemies/turtle_shell1.png");
		(*hostSprites)[c] = new Turtle(turtleAnim1, turtleAnim2, s1, s2);
		break;
	}

	case '5': {
		Animation *turtleAnim1 = new Animation;
		SDL_Surface* t1 = loadImage("images/enemies/turtle1.png");
		SDL_Surface* t2 = loadImage("images/enemies/turtle2.png");
		turtleAnim1->addFrame(t1, 400);
		turtleAnim1->addFrame(t2, 400);

		Animation *turtleAnim2 = new Animation;
		turtleAnim2->addFrame(getMirrorImage(t1), 400);
		turtleAnim2->addFrame(getMirrorImage(t2), 400);

		Animation *turtleAnim3 = new Animation;
		SDL_Surface* t3 = loadImage("images/enemies/turtle_flying1.png");
		SDL_Surface* t4 = loadImage("images/enemies/turtle_flying2.png");
		turtleAnim3->addFrame(t3, 400);
		turtleAnim3->addFrame(t4, 400);

		Animation *turtleAnim4 = new Animation;
		turtleAnim4->addFrame(getMirrorImage(t3), 400);
		turtleAnim4->addFrame(getMirrorImage(t4), 400);

		SDL_Surface* s1 = loadImage("images/enemies/turtle_shell2.png");
		SDL_Surface* s2 = loadImage("images/enemies/turtle_shell1.png");
		(*hostSprites)[c] = new Turtle(turtleAnim1, turtleAnim2, turtleAnim3,
				turtleAnim4, s1, s2);
		break;
	}
	}
}

PlayerAnimations* ResourceManager::getPlayerAnimations(char* walk1,
		char* walk2, char* walk3, char* dead, char* right, char* rightJump) {
	const int w = 2;
	const int h = 3;
	SDL_Surface* images[w][h];

	images[0][0] = loadImage(walk1);
	images[0][1] = loadImage(walk2);
	images[0][2] = loadImage(walk3);

	for (int i = 0; i < h; i++) {
		images[1][i] = getMirrorImage(images[0][i]);
	}
	const int length = 2;
	Animation* playerAnim[length];
	for (int i = 0; i < length; i++) {
		playerAnim[i] = createPlayerAnim(images[i][0], images[i][1],
				images[i][2]);
	}

	PlayerAnimations *anims = new PlayerAnimations;
	anims->dead = loadImage(dead);

	anims->right = loadImage(right);
	anims->left = getMirrorImage(anims->right);
	anims->jumpRight = loadImage(rightJump);
	anims->jumpLeft = getMirrorImage(anims->jumpRight);

	anims->rightWalking = playerAnim[0];
	anims->leftWalking = playerAnim[1];
	return anims;
}

void ResourceManager::loadPlayerSprite() {

	PlayerData *playerData = new PlayerData;
	playerData->smallAnim = getPlayerAnimations(
			"images/Mario/Mario - Walk1.png", "images/Mario/Mario - Walk2.png",
			"images/Mario/Mario - Walk3.png", "images/Mario/Mario - Dead.png",
			"images/Mario/Mario.png", "images/Mario/Mario - Jump.png");

	playerData->superAnim = getPlayerAnimations(
			"images/Mario/Super Mario - Walk1.png",
			"images/Mario/Super Mario - Walk2.png",
			"images/Mario/Super Mario - Walk3.png",
			"images/Mario/Mario - Dead.png", "images/Mario/Super Mario.png",
			"images/Mario/Super Mario - Jump.png");

	playerData->fieryAnim = getPlayerAnimations(
			"images/Mario/Fiery Mario - Walk1.png",
			"images/Mario/Fiery Mario - Walk2.png",
			"images/Mario/Fiery Mario - Walk3.png",
			"images/Mario/Mario - Dead.png", "images/Mario/Fiery Mario.png",
			"images/Mario/Fiery Mario - Jump.png");
	playerData->currentAnim = playerData->smallAnim;

	playerSprite = new Player(playerData);
}

bool ResourceManager::addTile(TileMap* map, int tileX, int tileY, char c) {
	//static int bleble = 0;
	TilesMapIter iter = hostTiles->find(c);

	if (iter != hostTiles->end()) {
		Tile *tile = iter->second->clone();
		map->setTile(tileX, tileY, tile);

		return true;
	}

	return false;

}

bool ResourceManager::addSprite(TileMap* map, int tileX, int tileY, char c) {

	SpritesMapIter iter = hostSprites->find(c);
	if (iter != hostSprites->end()) {

		Sprite *sprite = iter->second->clone();

		sprite->setX(TileMapRenderer::tilesToPixels(tileX)
				+ (TileMapRenderer::tilesToPixels(1) - sprite->getWidth()) / 2);

		sprite->setY(TileMapRenderer::tilesToPixels(tileY + 1)
				- sprite->getHeight());

		map->addSprite(sprite);
		return true;
	}

	return false;
}

Animation* ResourceManager::createPlayerAnim(SDL_Surface* player1,
		SDL_Surface* player2, SDL_Surface* player3) {
	Animation *anim = new Animation;
	anim->addFrame(player1, 150);
	anim->addFrame(player2, 150);
	anim->addFrame(player3, 150);
	return anim;

}

Animation* ResourceManager::createFlyAnim(SDL_Surface* img1, SDL_Surface* img2,
		SDL_Surface* img3) {
	Animation *anim = new Animation();
	anim->addFrame(img1, 100);
	anim->addFrame(img2, 100);
	anim->addFrame(img3, 100);
	anim->addFrame(img2, 100);
	return anim;

}

Animation* ResourceManager::createMashroomAnim(SDL_Surface* img1,
		SDL_Surface* img2) {
	Animation *anim = new Animation();
	anim->addFrame(img1, 500);
	anim->addFrame(img2, 500);
	return anim;
}

void ResourceManager::destroyHosts() {
	if (hostTiles != NULL) {
		for (TilesMapIter iter = hostTiles->begin(); iter != hostTiles->end(); iter++) {
			iter->second->cleanHost();
			delete iter->second;
		}
		hostTiles->clear();
		hostTiles = NULL;
	}

	if (hostSprites != NULL) {
		for (SpritesMapIter iter = hostSprites->begin(); iter
				!= hostSprites->end(); iter++) {
			iter->second->cleanHost();
			delete iter->second;
		}
		hostSprites->clear();
		hostSprites = NULL;
	}
}

void ResourceManager::destroyPlayer() {

	playerSprite->cleanHost();
	delete playerSprite;
	playerSprite = NULL;
}

void ResourceManager::loadHostAdditionalSprites() {
	if (hostAdditionalSprites == NULL)
		hostAdditionalSprites = new AdditionalSpritesMap;

	Animation *boxStarAnim = new Animation;
	boxStarAnim->addFrame(loadImage("images/goodies/box_star1.PNG"), 50);
	boxStarAnim->addFrame(loadImage("images/goodies/box_star2.PNG"), 50);
	boxStarAnim->addFrame(loadImage("images/goodies/box_star3.PNG"), 50);
	boxStarAnim->addFrame(loadImage("images/goodies/box_star4.PNG"), 50);
	(*hostAdditionalSprites)[(int) SPRITE_STAR] = new BoxStar(boxStarAnim);

	Animation *mashroomAnim = new Animation;
	mashroomAnim->addFrame(loadImage("images/goodies/mashroom.PNG"), 100);
	(*hostAdditionalSprites)[(int) SPRITE_MASHROOM]
			= new Mashroom(mashroomAnim);

	Animation *flowerAnim = new Animation;
	flowerAnim->addFrame(loadImage("images/goodies/flower1.PNG"), 50);
	flowerAnim->addFrame(loadImage("images/goodies/flower2.PNG"), 50);
	flowerAnim->addFrame(loadImage("images/goodies/flower3.PNG"), 50);
	flowerAnim->addFrame(loadImage("images/goodies/flower4.PNG"), 50);
	(*hostAdditionalSprites)[(int) SPRITE_FLOWER] = new Flower(flowerAnim);

	Animation *fireballAnim = new Animation;
	fireballAnim->addFrame(loadImage("images/goodies/ball1.PNG"), 50);
	fireballAnim->addFrame(loadImage("images/goodies/ball2.PNG"), 50);
	fireballAnim->addFrame(loadImage("images/goodies/ball3.PNG"), 50);
	fireballAnim->addFrame(loadImage("images/goodies/ball4.PNG"), 50);
	(*hostAdditionalSprites)[(int) SPRITE_FIREBALL]
			= new Fireball(fireballAnim);

}

Sprite* ResourceManager::getAdditionalSprite(int spriteID) {
	AdditionalSpritesMapIter iter = hostAdditionalSprites->find(spriteID);
	if (iter != hostAdditionalSprites->end()) {
		return iter->second->clone();
	}
	return NULL;
}

BrokenBricks* ResourceManager::getBrokenBricks() {
	return brokenBricks->clone();
}

void ResourceManager::loadBrokenBricks() {
	SDL_Surface* one = loadImage("images/map/cegla1.PNG");
	SDL_Surface* two = loadImage("images/map/cegla2.PNG");
	SDL_Surface* three = loadImage("images/map/cegla3.PNG");
	SDL_Surface* four = loadImage("images/map/cegla4.PNG");

	brokenBricks = new BrokenBricks(one, two, three, four);
}
