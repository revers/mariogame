/*
 * Plant.cpp
 *
 *  Created on: 2009-12-11
 *      Author: Revers
 */

#include "Player.h"
#include "Plant.h"

Plant::Plant(Animation *anim) :
	Creature(NULL, NULL, NULL) {
	this->anim = anim;
	image = anim->getImage();
	movable = false;
	movingState = STATE_DOWN;
	setVelocityY(0.03);
}

Plant::~Plant() {
}

void Plant::update(int elapsedTime) {
	anim->update(elapsedTime);
	image = anim->getImage();
	y = getY() + elapsedTime * getVelocityY();
	if (movingState == STATE_DOWN && getY() >= yBasePosition + getHeight() + 40) {
		setVelocityY(-getVelocityY());
		movingState = STATE_UP;
	} else if (movingState == STATE_UP && getY() <= yBasePosition) {
		setVelocityY(-getVelocityY());
		movingState = STATE_DOWN;
	}

}

void Plant::cleanHost() {
	anim->cleanHost();
}

float Plant::getMaxSpeed() {
	return 0.0f;
}

void Plant::handleCollision(Player *player, int side) {
	player->hurt();
}
