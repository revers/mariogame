#include <iostream>
using namespace std;

#include "GameManager.h"
#include "Constants.h"

int main(int argc, char **argv) {

	freopen("CON", "wt", stdout);
	freopen("CON", "wt", stderr);

	if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
		return false;
	}
	SDL_Surface* screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT,
			SCREEN_BPP, SDL_SWSURFACE | SDL_DOUBLEBUF);

	if (screen == NULL) {
		return false;
	}

	GameManager gm(screen);
	gm.run();
	return 0;
}
