/*
 * Fireball.h
 *
 *  Created on: 2009-12-08
 *      Author: Revers
 */

#ifndef FIREBALL_H_
#define FIREBALL_H_
#include "Sprite.h"

class Fireball: public Sprite {
	static const int LIFE_TIME = 1000;
	int durationTime;
	int direction;
public:
	Fireball(Animation *anim);
	virtual ~Fireball();
	virtual void update(int elapsedTime);

	virtual void collideVertical();

	virtual inline Sprite* clone();
	virtual inline void cleanHost();

	inline void setDirection(int direction);
};

inline void Fireball::setDirection(int direction) {
	this->direction = direction;
}

inline Sprite* Fireball::clone() {
	return new Fireball(anim->clone());
}

inline void Fireball::cleanHost() {
	anim->cleanHost();
}

#endif /* FIREBALL_H_ */
