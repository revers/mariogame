/*
 * Tile.cpp
 *
 *  Created on: 2009-12-05
 *      Author: Revers
 */

#include "Tile.h"
#include <iostream>

Tile::Tile() {
	x = 0;
	y = 0;
	width = 0;
	height = 0;
	image = NULL;
	visible = false;
	destroyed = false;
}

Tile::Tile(SDL_Surface* image) : image(image) {
	x = 0;
	y = 0;
	width = image->w;
	height = image->h;
	visible = true;
	destroyed = false;
}

Tile::~Tile() {
}

Tile* Tile::clone() {
	return new Tile(image);
}

void Tile::cleanHost() {
	SDL_FreeSurface(image);
}
