/*
 * PipeEntranceTile.cpp
 *
 *  Created on: 2009-12-09
 *      Author: Revers
 */

#include "PipeEntranceTile.h"
#include "Constants.h"

PipeEntranceTile::PipeEntranceTile(SDL_Surface* image, int side) :
	Tile(image) {
	this->side = side;
}

PipeEntranceTile::~PipeEntranceTile() {
}

Tile* PipeEntranceTile::clone() {
	return new PipeEntranceTile(image, side);
}

#define MARGIN 5
bool PipeEntranceTile::canEnter(int x, int y, int width, int height) {
	switch (side) {
	case LEFT:
		if ((x >= getX() * TILE_SIZE + MARGIN) && (x + width <= getX()
				* TILE_SIZE + 2 * TILE_SIZE - MARGIN))
			return true;
		return false;
	case RIGHT:
		if ((x >= getX() * TILE_SIZE - TILE_SIZE + MARGIN) && (x + width
				<= getX() * TILE_SIZE + TILE_SIZE - MARGIN))
			return true;
		return false;
	case TOP:
		if ((y >= getY() * TILE_SIZE) && (y + height <= getY() * TILE_SIZE + 2
				* TILE_SIZE))
			return true;
		return false;
	case BOTTOM:
		if ((y >= getY() * TILE_SIZE - TILE_SIZE) && (y + height <= getY()
				* TILE_SIZE + TILE_SIZE))
			return true;
		return false;
	}
	return false;
}

bool PipeEntranceTile::isSamePipe(int pipe1X, int pipe1Y, int pipe2X,
		int pipe2Y, int pipe1Side, int pipe2Side) {

	bool isSame = (pipe1Side == RIGHT) && (pipe2Side == RIGHT) && (pipe1X
			== pipe2X) && (pipe1Y == pipe2Y || pipe1Y == pipe2Y + 1);

	return isSame || ((pipe1Side == BOTTOM) && (pipe2Side == BOTTOM) && (pipe1Y
			== pipe2Y) && (pipe1X == pipe2X || pipe1X == pipe2X + 1));
}
