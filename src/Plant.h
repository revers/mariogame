/*
 * Plant.h
 *
 *  Created on: 2009-12-11
 *      Author: Revers
 */

#ifndef PLANT_H_
#define PLANT_H_

#include "Creature.h"
#include "Constants.h"

class Plant: public Creature {
	static const int STATE_UP = 0;
	static const int STATE_DOWN = 1;
	int movingState;
	int yBasePosition;
public:
	Plant(Animation *anim);
	virtual ~Plant();

	virtual float getMaxSpeed();
	inline Creature* clone();

	virtual void update(int elapsedTime);
	virtual void cleanHost();
	virtual void handleCollision(Player *player, int side);

	virtual inline void setX(float x);
	virtual inline void setY(float y);

	//	virtual inline int getPlayerCollisionReaction(int side);
};



//inline int Plant::getPlayerCollisionReaction(int side) {
//	return REACTION_PLAYER_HURT;
//}

inline void Plant::setY(float y) {
	yBasePosition = (int) y;
	Creature::setY(y);
}

inline void Plant::setX(float x) {
	Creature::setX(x + (float) (TILE_SIZE / 2));
}

inline Creature* Plant::clone() {
	return new Plant(anim->clone());
}

#endif /* PLANT_H_ */
