/*
 * BricksTile.cpp
 *
 *  Created on: 2009-12-07
 *      Author: Revers
 */

#include "BricksTile.h"

BricksTile::BricksTile(SDL_Surface* image) :
	Tile(image) {

}

BricksTile::~BricksTile() {
}

Tile* BricksTile::clone() {
	return new BricksTile(image);
}

void BricksTile::cleanHost() {
	SDL_FreeSurface(image);
}
