/*
 * ResourceManager.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef RESOURCEMANAGER_H_
#define RESOURCEMANAGER_H_

#include <string>
#include <vector>
#include <map>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Animation.h"
#include "Sprite.h"
#include "TileMap.h"
#include "Tile.h"
#include "BrokenBricks.h"
#include "Entries.h"

#define IGNORED_RED 0
#define IGNORED_GREEN 255
#define IGNORED_BLUE 255

typedef std::map<char, Tile*> TilesMap;
typedef TilesMap::const_iterator TilesMapIter;

typedef std::map<char, Sprite*> SpritesMap;
typedef SpritesMap::const_iterator SpritesMapIter;

typedef std::map<int, Sprite*> AdditionalSpritesMap;
typedef AdditionalSpritesMap::const_iterator AdditionalSpritesMapIter;

class ResourceManager {
private:
	TilesMap *hostTiles;
	SpritesMap *hostSprites;
	AdditionalSpritesMap *hostAdditionalSprites;
	int currentMap;
	Sprite* playerSprite;
	BrokenBricks* brokenBricks;
	SDL_Surface* heartImage;

	Uint32 getPixel(SDL_Surface *surface, int x, int y);
	void putPixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
	SDL_Surface* flipSurface(SDL_Surface *surface, int flags);

	bool addSprite(TileMap* map, int tileX, int tileY, char c);
	bool addTile(TileMap* map, int tileX, int tileY, char c);
	void loadHostTile(char c);
	void loadHostGoodie(char c);
	void loadHostEnemy(char c);
	void loadPlayerSprite();
	void loadHostAdditionalSprites();
	void loadBrokenBricks();

	PipeEntry* getPipeEntry(std::string *entrArr);
	NextLevelEntry* getNextLevelEntry(std::string *nextLvlArr);

	PlayerAnimations* getPlayerAnimations(char* walk1, char* walk2,
			char* walk3, char* dead, char* right, char* rightJump);
	Animation* createPlayerAnim(SDL_Surface* player1, SDL_Surface* player2,
			SDL_Surface* player3);
	Animation* createFlyAnim(SDL_Surface* img1, SDL_Surface* img2,
			SDL_Surface* img3);
	Animation* createMashroomAnim(SDL_Surface* img1, SDL_Surface* img2);

	void destroyHosts();
	void destroyPlayer();

public:
	static const int FLIP_VERTICAL = 1;
	static const int FLIP_HORIZONTAL = 2;

	static const int SPRITE_STAR = 0;
	static const int SPRITE_MASHROOM = 1;
	static const int SPRITE_FLOWER = 2;
	static const int SPRITE_FIREBALL = 3;

	ResourceManager();
	virtual ~ResourceManager();
	SDL_Surface* loadImage(std::string filename);

	TileMap* loadMap(std::string filename);
	TileMap* loadNextMap();
	TileMap* reloadMap();

	Sprite* getAdditionalSprite(int spriteID);
	BrokenBricks* getBrokenBricks();

	inline SDL_Surface* getHeartImage();

	inline SDL_Surface* getMirrorImage(SDL_Surface* image);
	inline SDL_Surface* getFlippedImage(SDL_Surface* image);
	inline int getCurrentMap();
	inline void setCurrentMap(int currentMap);
};

inline SDL_Surface* ResourceManager::getMirrorImage(SDL_Surface* image) {
	return flipSurface(image, FLIP_HORIZONTAL);
}
inline SDL_Surface* ResourceManager::getFlippedImage(SDL_Surface* image) {
	return flipSurface(image, FLIP_VERTICAL);
}

inline int ResourceManager::getCurrentMap() {
	return currentMap;
}

inline void ResourceManager::setCurrentMap(int currentMap) {
	this->currentMap = currentMap;
}

inline SDL_Surface* ResourceManager::getHeartImage() {
	return heartImage;
}

#endif /* RESOURCEMANAGER_H_ */
