/*
 * PowerUp.cpp
 *
 *  Created on: 2009-12-06
 *      Author: Revers
 */

#include "Goodie.h"

void BoxStar::update(int elapsedTime) {
	anim->update(elapsedTime);

	if(totalDuration > anim->getAnimTime())
		expired = true;

	totalDuration += elapsedTime;
	image = anim->getImage();
}
