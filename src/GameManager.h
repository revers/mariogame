/*
 * GameManager.h
 *
 *  Created on: 2009-11-30
 *      Author: Revers
 */

#ifndef GAMEMANAGER_H_
#define GAMEMANAGER_H_

#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "InputManager.h"
#include "Timer.h"
#include "TileMap.h"
#include "GameAction.h"
#include "ResourceManager.h"
#include "TileMapRenderer.h"
#include "Player.h"
#include "Goodie.h"

#define TITLE "Prawie ze mario ;) "
using namespace std;

struct Point {
	int x;
	int y;
};

#define COLL_TILES_MAX_SIZE 10
class CollisionTiles {
private:
	int size;
	int dummyIndex;
	Tile* tiles[COLL_TILES_MAX_SIZE];
	Tile* dummyTiles[COLL_TILES_MAX_SIZE];
public:
	CollisionTiles() {
		size = 0;
		dummyIndex = 0;
		for (int i = 0; i < COLL_TILES_MAX_SIZE; i++)
			dummyTiles[i] = new Tile;
	}

	~CollisionTiles() {
		for (int i = 0; i < COLL_TILES_MAX_SIZE; i++)
			delete dummyTiles[i];
	}

	void clean() {
		size = 0;
		dummyIndex = 0;
	}

	void addTile(Tile* tile) {
		tiles[size++] = tile;
	}

	void addDummyTile(int x, int y) {
		dummyTiles[dummyIndex]->setX(x);
		dummyTiles[dummyIndex]->setY(y);
		tiles[size++] = dummyTiles[dummyIndex++];
	}

	Tile* getTile(int index) {
		return tiles[index];
	}

	int getSize() {
		return size;
	}
};

class GameManager {
private:
	static const int ENTRANCE_SUBMAP = 0;
	static const int ENTRANCE_NEXT_LEVEL = 1;

	bool mapChanged;
	SDL_Event event;
	bool isRunning;
	SDL_Surface *screen;
	InputManager inputMgr;
	TileMap *map;
	ResourceManager resourceManager;
	TileMapRenderer renderer;
	GameAction moveLeft;
	GameAction moveRight;
	GameAction jump;
	GameAction exit;
	GameAction shoot;
	GameAction moveDown;

//	GameAction pause;
//	GameAction enter;

	CollisionTiles *collisionTiles;

	TTF_Font *normalFont;
	TTF_Font *bigFont;
	SDL_Surface *textImage;
	//SDL_Surface *connsTextBig;
	int entranceType;
	PipeEntry *pipeEntry;
	bool isSubmap;
	TileMap *oldMap;

	void resetButtons();
	void checkInput(int elapsedTime);
	void updateMovableSprite(Sprite *sprite, int elapsedTime);
	CollisionTiles* getTileCollision(Sprite *sprite, float newX, float newY);
	bool isCollision(Sprite *s1, Sprite *s2);
	Sprite* getSpriteCollision(Sprite *sprite);
	void checkPlayerCollision(Player *player, int side);
	void checkPipeEntranceCollision(Player *player, CollisionTiles* collTiles,
			int side);
	void checkSpecialTileCollision(Player *player, CollisionTiles* collTiles);

	void acquireGoodie(Goodie *goodie);
	void update(int elapsedTime);
	void draw(int elapsedTime);
	//void drawString(std::string str, int x, int y);

	void drawString(std::string str, int x, int y, TTF_Font *font,
			SDL_Color* color1, SDL_Color* color2);

	void checkCreatureCollision(Creature *creature);
public:
	GameManager(SDL_Surface *screen);
	virtual ~GameManager();
	void run();
	bool init();
	void gameLoop();

	inline TileMap* getMap();
};

inline TileMap* GameManager::getMap() {
	return map;
}

#endif /* GAMEMANAGER_H_ */
