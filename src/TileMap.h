/*
 * TileMap.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef TILEMAP_H_
#define TILEMAP_H_

#include <list>
#include <cassert>
#include "SDL/SDL.h"
#include "Sprite.h"
#include "Tile.h"
#include "Player.h"
#include "BrokenBricks.h"
#include "Entries.h"

typedef std::list<Sprite*> SpriteList;
typedef SpriteList::iterator SpriteListItor;

typedef std::list<BrokenBricks*> BrokenBricksList;
typedef BrokenBricksList::iterator BrokenBricksListIter;

typedef std::list<PipeEntry*> PipeEntryList;
typedef PipeEntryList::iterator PipeEntryListIter;

class TileMap {
private:
	PipeEntryList *pipeEntryList;
	NextLevelEntry *nextLevelEntry;
	Tile* **tiles;
	SpriteList *sprites;
	BrokenBricksList *brokenBricksList;
	Player *player;
	int width;
	int height;
	SDL_Surface *background;
public:
	TileMap(int width, int height);
	virtual ~TileMap();

	Tile* getTile(int x, int y);
	void update(int elapsedTime);

	inline void setTile(int x, int y, Tile* tile);
	inline int getWidth();
	inline int getHeight();
	inline Player* getPlayer();
	inline void setPlayer(Player *player);
	inline void addSprite(Sprite *sprite);
	inline void removeSprite(Sprite *sprite);
	inline SpriteListItor getBeginSprite();
	inline SpriteListItor getEndSprite();

	inline SpriteListItor eraseSpriteIter(SpriteListItor &itor);
	inline void setBackground(SDL_Surface* background);
	inline SDL_Surface* getBackground();

	inline BrokenBricksList* getBrokenBricksList();
	inline void addBrokenBricks(BrokenBricks* brokenBricks);

	inline PipeEntryList* getPipeEntryList();
	inline void setPipeEntryList(PipeEntryList* pipeEntryList);
	inline void addPipeEntry(PipeEntry* pipeEntry);
	inline void setNextLevelEntry(NextLevelEntry* nextLevelEntry);
	inline NextLevelEntry* getNextLevelEntry();

	void removePipeEntry(PipeEntry* pipeEntry);
};

inline NextLevelEntry* TileMap::getNextLevelEntry() {
	return nextLevelEntry;
}

inline void TileMap::setPipeEntryList(PipeEntryList* pipeEntryList) {
	this->pipeEntryList = pipeEntryList;
}

inline PipeEntryList* TileMap::getPipeEntryList() {
	return pipeEntryList;
}
inline void TileMap::addPipeEntry(PipeEntry* pipeEntry) {
	pipeEntryList->push_back(pipeEntry);
}
inline void TileMap::setNextLevelEntry(NextLevelEntry* nextLevelEntry) {
	this->nextLevelEntry = nextLevelEntry;
}

inline void TileMap::addBrokenBricks(BrokenBricks* brokenBricks) {
	brokenBricksList->push_back(brokenBricks);
}

inline BrokenBricksList* TileMap::getBrokenBricksList() {
	return brokenBricksList;
}

inline SDL_Surface* TileMap::getBackground() {
	return background;
}

inline void TileMap::setBackground(SDL_Surface* background) {
	this->background = background;
}

inline SpriteListItor TileMap::getEndSprite() {
	return sprites->end();
}

inline SpriteListItor TileMap::eraseSpriteIter(SpriteListItor &itor) {
	delete (*itor);
	return sprites->erase(itor);
}

inline SpriteListItor TileMap::getBeginSprite() {
	return sprites->begin();
}

inline Player* TileMap::getPlayer() {
	return player;
}

inline void TileMap::setPlayer(Player *player) {
	this->player = player;
}
inline void TileMap::addSprite(Sprite *sprite) {
	sprites->push_back(sprite);
}
inline void TileMap::removeSprite(Sprite *sprite) {
	sprites->remove(sprite);
	//delete sprite;
}

inline int TileMap::getWidth() {
	return width;
}
inline int TileMap::getHeight() {
	return height;
}

inline void TileMap::setTile(int x, int y, Tile* tile) {
	assert(x >= 0 && x < width && y >= 0 && y < height && "Przekroczony zakres w TileMap!");
	tiles[x][y] = tile;
}
#endif /* TILEMAP_H_ */
