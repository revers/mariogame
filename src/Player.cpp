/*
 * Player.cpp
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#include "Player.h"
#include "Constants.h"

#define SMALL playerData->smallAnim
#define SUPER playerData->superAnim
#define FIERY playerData->fieryAnim
#define CURR_ANIM playerData->currentAnim

Player::Player(PlayerData *playerData) :
	Creature(SMALL->leftWalking, SMALL->rightWalking, SMALL->dead) {
	this->playerData = playerData;
	direction = RIGHT;
	ignoringControls = false;
	//flying = false;

	if (CURR_ANIM == SMALL) {
		width = 40;
		height = 40;
		this->state = STATE_NORMAL;
	} else if (CURR_ANIM == SUPER) {
		width = 40;
		height = 80;
		this->state = STATE_SUPER;
	} else if (CURR_ANIM == FIERY) {
		width = 40;
		height = 80;
		this->state = STATE_FIERY;
	}
}

Player::~Player() {
}

void Player::setY(float y) {
	if (Math::round(y) > Math::round(getY())) {
		onGround = false;
	}
	Sprite::setY(y);
}

void Player::collideVertical() {
	// check if collided with ground
	if (getVelocityY() > 0) {
		onGround = true;
	}
	setVelocityY(0);
}

void Player::jump(bool forceJump) {
	if (onGround || forceJump) {
		onGround = false;
		setVelocityY(JUMP_SPEED);
	}
}

void Player::cleanHost() {
	cleanHostAnimations(playerData->smallAnim);
	cleanHostAnimations(playerData->superAnim);
	cleanHostAnimations(playerData->fieryAnim);
	delete playerData;
}

void Player::cleanHostAnimations(PlayerAnimations *pAnim) {
	SDL_FreeSurface(pAnim->left);
	SDL_FreeSurface(pAnim->left);
	SDL_FreeSurface(pAnim->jumpLeft);
	SDL_FreeSurface(pAnim->jumpRight);
	SDL_FreeSurface(pAnim->dead);
	pAnim->leftWalking->cleanHost();
	delete pAnim->leftWalking;
	pAnim->rightWalking->cleanHost();
	delete pAnim->rightWalking;
	delete pAnim;
}

void Player::update(int elapsedTime) {

	if (getVelocityX() < 0) {
		direction = LEFT;
	} else if (getVelocityX() > 0) {
		direction = RIGHT;
	}

	if (getVelocityY() != 0 && state > STATE_DYING && state <= STATE_FIERY) {
		if (direction == LEFT)
			image = CURR_ANIM->jumpLeft;
		else
			image = CURR_ANIM->jumpRight;
	} else if (getVelocityX() == 0 && state > STATE_DYING) {
		if (direction == LEFT)
			image = CURR_ANIM->left;
		else
			image = CURR_ANIM->right;
	} else if (state == STATE_DYING) {
		image = CURR_ANIM->dead;
	} else {
		Animation *newAnim = anim;
		if (direction == LEFT)
			newAnim = CURR_ANIM->leftWalking;
		else
			newAnim = CURR_ANIM->rightWalking;

		// update the Animation
		if (anim != newAnim) {
			anim = newAnim;
			anim->start();
		} else {
			anim->update(elapsedTime);
		}
		image = anim->getImage();
	}

	stateTime += elapsedTime;
	if (state == STATE_HURT) {
		if (image != NULL)
			oldImage = image;

		if ((stateTime / 100) % 2 == 0)
			image = NULL;
		else
			image = oldImage;
		if (stateTime >= HURT_TIME) {
			setState(oldState);
		}
	} else if (state == STATE_DYING && stateTime >= DIE_TIME) {
		setState(STATE_DEAD);
		decreaseLives();
	} else if (state == STATE_ENTERING && stateTime >= ENTRANCE_TIME) {
		setState(STATE_ENTERED);
	} else if (state == STATE_EXITING && getY() + getHeight() < exitEndY) {

		setState(oldState);
		setY(exitEndY - getHeight());

		setVelocityY(0.0);

		ignoringControls = false;
	}
}

void Player::setState(int state) {
	if (this->state != state) {
		oldState = this->state;
		this->state = state;
		stateTime = 0;
		switch (state) {
		case STATE_DYING:
			setVelocityX(0);
			setVelocityY(-0.4);
			height = 35;
			break;
		case STATE_NORMAL:
			CURR_ANIM = SMALL;
			height = 40;
			break;
		case STATE_SUPER:
			if (CURR_ANIM == SMALL) {
				height = 80;
				setY(getY() - 40);
			}
			CURR_ANIM = SUPER;
			break;
		case STATE_FIERY:
			if (CURR_ANIM == SMALL) {
				height = 80;
				setY(getY() - 40);
			}
			CURR_ANIM = FIERY;
			break;
		case STATE_HURT:
			if (oldState == STATE_NORMAL) {
				setState(STATE_DYING);
				break;
			} else if (oldState == STATE_SUPER)
				setState(STATE_NORMAL);
			else if (oldState == STATE_FIERY)
				setState(STATE_SUPER);
			oldState = this->state;
			this->state = STATE_HURT;
			break;
		case STATE_ENTERING:
			//flying = true;
			ignoringControls = true;
			if (entranceSide == BOTTOM) {
				setVelocityX(0.0);
				setVelocityY(0.1);
			} else {
				setVelocityX(0.05);
				setVelocityY(0.0);
			}
			break;
		case STATE_EXITING:
			ignoringControls = true;
			setVelocityX(0.0);
			setVelocityY(-0.1);
			exitEndY = (int) getY();
		}
	}
}

int Player::getHeight() {
	return height;
}

int Player::getWidth() {
	return width;
}

void Player::updatePosition(int elapsedTime) {
	setX(getX() + getVelocityX() * elapsedTime);
	setY(getY() + getVelocityY() * elapsedTime);
}
