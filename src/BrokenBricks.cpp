/*
 * BrokenBricks.cpp
 *
 *  Created on: 2009-12-08
 *      Author: Revers
 */

#include "BrokenBricks.h"
#include "Constants.h"

BrokenBricks::BrokenBricks(Brick* brickOne, Brick* brickTwo, Brick* brickThree,
		Brick* brickFour) {
	this->brickOne = brickOne;
	this->brickTwo = brickTwo;
	this->brickThree = brickThree;
	this->brickFour = brickFour;
	this->expired = false;

	brickOne->setVelocityX(-0.3);
	brickOne->setVelocityY(-0.4);

	brickTwo->setVelocityX(0.3);
	brickTwo->setVelocityY(-0.4);

	brickThree->setVelocityX(-0.3);
	brickThree->setVelocityY(-0.3);

	brickFour->setVelocityX(0.3);
	brickFour->setVelocityY(-0.3);
}

BrokenBricks::BrokenBricks(SDL_Surface* one, SDL_Surface* two,
		SDL_Surface* three, SDL_Surface* four) {
	this->brickOne = new Brick(one);
	this->brickTwo = new Brick(two);
	this->brickThree = new Brick(three);
	this->brickFour = new Brick(four);
}

BrokenBricks::~BrokenBricks() {
	delete brickOne;
	delete brickTwo;
	delete brickThree;
	delete brickFour;
}

void BrokenBricks::update(int elapsedTime) {
	if ((brickOne->getY() > SCREEN_HEIGHT)
			&& (brickTwo->getY() > SCREEN_HEIGHT)) {
		expired = true;
		return;
	}

	brickOne->setVelocityY(brickOne->getVelocityY() + GRAVITY * elapsedTime);
	brickTwo->setVelocityY(brickTwo->getVelocityY() + GRAVITY * elapsedTime);
	brickThree->setVelocityY(brickThree->getVelocityY() + GRAVITY * elapsedTime);
	brickFour->setVelocityY(brickFour->getVelocityY() + GRAVITY * elapsedTime);

	float newX = brickOne->getX() + brickOne->getVelocityX() * elapsedTime;
	float newY = brickOne->getY() + brickOne->getVelocityY() * elapsedTime;
	brickOne->setX(newX);
	brickOne->setY(newY);

	newX = brickTwo->getX() + brickTwo->getVelocityX() * elapsedTime;
	newY = brickTwo->getY() + brickTwo->getVelocityY() * elapsedTime;
	brickTwo->setX(newX);
	brickTwo->setY(newY);

	newX = brickThree->getX() + brickThree->getVelocityX() * elapsedTime;
	newY = brickThree->getY() + brickThree->getVelocityY() * elapsedTime;
	brickThree->setX(newX);
	brickThree->setY(newY);

	newX = brickFour->getX() + brickFour->getVelocityX() * elapsedTime;
	newY = brickFour->getY() + brickFour->getVelocityY() * elapsedTime;
	brickFour->setX(newX);
	brickFour->setY(newY);
}

BrokenBricks* BrokenBricks::clone() {
	return new BrokenBricks((Brick*) brickOne->clone(),
			(Brick*) brickTwo->clone(), (Brick*) brickThree->clone(),
			(Brick*) brickFour->clone());
}

void BrokenBricks::cleanHost() {
	brickOne->cleanHost();
	brickTwo->cleanHost();
	brickThree->cleanHost();
	brickFour->cleanHost();
}

void BrokenBricks::setLocation(int x, int y) {
	brickOne->setX((float) x);
	brickOne->setY((float) y);

	brickTwo->setX((float) (x + brickOne->getImage()->w));
	brickTwo->setY((float) y);

	brickThree->setX((float) x);
	brickThree->setY((float) (y + brickOne->getImage()->h));

	brickFour->setX((float) (x + brickOne->getImage()->w));
	brickFour->setY((float) (y + brickOne->getImage()->h));
}
