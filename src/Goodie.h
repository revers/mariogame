/*
 * PowerUp.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef POWERUP_H_
#define POWERUP_H_
#include "Sprite.h"

//-----------------------------------------------------------------
class Goodie: public Sprite {
public:
	inline Goodie(Animation *anim);
	virtual ~Goodie() {
	}

	virtual Sprite* clone() = 0;
	virtual inline void cleanHost();
};

inline Goodie::Goodie(Animation *anim) :
	Sprite(anim) {
	movable = false;
}

inline void Goodie::cleanHost() {
	anim->cleanHost();
}
//=================================================================

//-----------------------------------------------------------------
class Star: public Goodie {
public:
	inline Star(Animation *anim);
	virtual inline Sprite* clone();
};

inline Star::Star(Animation *anim) :
	Goodie(anim) {
}

inline Sprite* Star::clone() {
	//std::cout << "star.clone()" << std::endl;
	return new Star(anim->clone());
}
//=================================================================

//-----------------------------------------------------------------
class Flower: public Goodie {
public:
	inline Flower(Animation *anim);
	virtual inline Sprite* clone();
};

inline Flower::Flower(Animation *anim) :
	Goodie(anim) {
	setVelocityY(0.0);
}

inline Sprite* Flower::clone() {
	return new Flower(anim->clone());
}
//=================================================================

//-----------------------------------------------------------------
class BoxStar: public Goodie {
	int totalDuration;
public:
	inline BoxStar(Animation *anim);
	virtual inline Sprite* clone();

	virtual void update(int elapsedTime);
};

inline BoxStar::BoxStar(Animation *anim) :
	Goodie(anim) {
	totalDuration = 0;
}

inline Sprite* BoxStar::clone() {
	//std::cout << "boxstar.clone()" << std::endl;
	return new BoxStar(anim->clone());
}
//=================================================================

//-----------------------------------------------------------------
class Mashroom: public Goodie {
public:
	inline Mashroom(Animation *anim);
	virtual inline Sprite* clone();
	virtual void update(int elapsedTime) {

	}
};

inline Mashroom::Mashroom(Animation *anim) :
	Goodie(anim) {
	setVelocityX(0.2f);
	setVelocityY(0.0f);
	movable = true;
}

inline Sprite* Mashroom::clone() {
	//std::cout << "mashroom.clone()" << std::endl;
	return new Mashroom(anim->clone());
}
//=================================================================


#endif /* POWERUP_H_ */
