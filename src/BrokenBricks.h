/*
 * BrokenBricks.h
 *
 *  Created on: 2009-12-08
 *      Author: Revers
 */

#ifndef BROKENBRICKS_H_
#define BROKENBRICKS_H_
#include "SDL/SDL.h"
#include "Sprite.h"

class BrokenBricks {
private:
	class Brick: public Sprite {
	public:
		Brick(SDL_Surface* image) :
			Sprite(NULL) {
			this->image = image;
		}
		virtual void update() {
		}
		virtual void cleanHost() {
			SDL_FreeSurface(image);
		}
		virtual Sprite* clone() {
			return new Brick(image);
		}
	};

	Brick* brickOne;
	Brick* brickTwo;
	Brick* brickThree;
	Brick* brickFour;
	bool expired;

	BrokenBricks(Brick* brickOne, Brick* brickTwo, Brick* brickThree,
			Brick* brickFour);
public:
	BrokenBricks(SDL_Surface* one, SDL_Surface* two, SDL_Surface* three,
			SDL_Surface* four);
	virtual ~BrokenBricks();

	void update(int elapsedTime);
	BrokenBricks* clone();
	void cleanHost();
	void setLocation(int x, int y);

	inline bool isExpired();
	inline Sprite* getOne();
	inline Sprite* getTwo();
	inline Sprite* getThree();
	inline Sprite* getFour();
};

inline Sprite* BrokenBricks::getOne() {
	return brickOne;
}
inline Sprite* BrokenBricks::getTwo() {
	return brickTwo;
}
inline Sprite* BrokenBricks::getThree() {
	return brickThree;
}
inline Sprite* BrokenBricks::getFour() {
	return brickFour;
}

inline bool BrokenBricks::isExpired() {
	return expired;
}

#endif /* BROKENBRICKS_H_ */
