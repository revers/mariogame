/*
 * BricksTile.h
 *
 *  Created on: 2009-12-07
 *      Author: Revers
 */

#ifndef BRICKSTILE_H_
#define BRICKSTILE_H_
#include "Tile.h"

class BricksTile: public Tile {
public:
	BricksTile(SDL_Surface *image);
	virtual ~BricksTile();

	virtual Tile* clone();
	virtual void cleanHost();
};

#endif /* BRICKSTILE_H_ */
