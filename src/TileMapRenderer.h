/*
 * TileMapRenderer.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef TILEMAPRENDERER_H_
#define TILEMAPRENDERER_H_
#include <cmath>

#include "SDL/SDL.h"
#include "TileMap.h"
#include "Creature.h"
#include "Constants.h"

class TileMapRenderer {
public:
	TileMapRenderer();
	virtual ~TileMapRenderer();

	void draw(SDL_Surface* screen, TileMap *map, int screenWidth,
			int screenHeight, int elapsedTime);
	static void applySurface(int x, int y, SDL_Surface* source,
			SDL_Surface* destination);

	static int pixelsToTiles(int pixels) {
		return (int) std::floor(((float) pixels / TILE_SIZE));
	}

	static int tilesToPixels(int numTiles) {
		return numTiles * TILE_SIZE;
	}
	static int pixelsToTiles(float pixels) {
		return pixelsToTiles(Math::round(pixels));
	}

};

#endif /* TILEMAPRENDERER_H_ */
