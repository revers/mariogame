/*
 * Entries.h
 *
 *  Created on: 2009-12-10
 *      Author: Revers
 */

#ifndef ENTRIES_H_
#define ENTRIES_H_

class PipeEntry {
	std::string *submapName;
	int entranceSide;
	int entranceX;
	int entranceY;
	int exitSide;
	int exitX;
	int exitY;
public:
	~PipeEntry() {
		delete submapName;
	}

	int getEntranceSide() const {
		return entranceSide;
	}

	int getEntranceX() const {
		return entranceX;
	}

	int getEntranceY() const {
		return entranceY;
	}

	int getExitSide() const {
		return exitSide;
	}

	int getExitX() const {
		return exitX;
	}

	int getExitY() const {
		return exitY;
	}

	std::string* getSubmapName() const {
		return submapName;
	}

	void setEntranceSide(int entranceSide) {
		this->entranceSide = entranceSide;
	}

	void setEntranceX(int entranceX) {
		this->entranceX = entranceX;
	}

	void setEntranceY(int entranceY) {
		this->entranceY = entranceY;
	}

	void setExitSide(int exitSide) {
		this->exitSide = exitSide;
	}

	void setExitX(int exitX) {
		this->exitX = exitX;
	}

	void setExitY(int exitY) {
		this->exitY = exitY;
	}

	void setSubmapName(std::string* submapName) {
		this->submapName = submapName;
	}
};

class NextLevelEntry {
	int x;
	int y;
	int side;
public:
	int getSide() const {
		return side;
	}

	int getX() const {
		return x;
	}

	int getY() const {
		return y;
	}

	void setSide(int side) {
		this->side = side;
	}

	void setX(int x) {
		this->x = x;
	}

	void setY(int y) {
		this->y = y;
	}
};

#endif /* ENTRIES_H_ */
