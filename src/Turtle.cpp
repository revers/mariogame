/*
 * Turtle.cpp
 *
 *  Created on: 2009-12-11
 *      Author: Revers
 */

#include "Player.h"
#include "Turtle.h"
#include "Constants.h"

Turtle::Turtle(Animation *left, Animation *right, SDL_Surface *shellOne,
		SDL_Surface *shellTwo) :
	Creature(left, right, NULL) {
	this->shellOne = shellOne;
	this->shellTwo = shellTwo;
	this->flyingRight = NULL;
	this->flyingLeft = NULL;
}

Turtle::Turtle(Animation *left, Animation *right, Animation *flyingLeft,
		Animation *flyingRight, SDL_Surface *shellOne, SDL_Surface *shellTwo) :
	Creature(left, right, NULL) {
	this->shellOne = shellOne;
	this->shellTwo = shellTwo;
	this->flyingRight = flyingRight;
	this->flyingLeft = flyingLeft;
	state = STATE_FLYING;
	image = flyingRight->getImage();
	setVelocityY(0.5);
	setVelocityX(0.12);//getMaxSpeed());
}

Turtle::~Turtle() {
}

Sprite* Turtle::clone() {
	if (state == STATE_FLYING) {
		return new Turtle(leftWalking, rightWalking, flyingLeft, flyingRight,
				shellOne, shellTwo);
	}

	return new Turtle(leftWalking, rightWalking, shellOne, shellTwo);
}

void Turtle::update(int elapsedTime) {
	if(state == STATE_DEAD)
		return;

	if (state != STATE_SHELL && state != STATE_MOVING_SHELL) {
		//Creature::update(elapsedTime);
		Animation *newAnim = anim;
		if (getVelocityX() < 0) {
			if (state == STATE_NORMAL)
				newAnim = leftWalking;
			else
				newAnim = flyingLeft;
		} else if (getVelocityX() > 0) {
			if (state == STATE_NORMAL)
				newAnim = rightWalking;
			else
				newAnim = flyingRight;
		}

		if (anim != newAnim) {
			anim = newAnim;
			anim->start();
		} else {
			anim->update(elapsedTime);
		}

		image = anim->getImage();
	} else if (state == STATE_SHELL && stateTime >= 2000) {
		if ((stateTime / 400) % 2 == 0)
			image = shellOne;
		else
			image = shellTwo;
		if (stateTime >= 4000) {
			setState(STATE_NORMAL);
			float y = getY() - (rightWalking->getImage()->h - getHeight());
			image = rightWalking->getImage();
			setY(y);
		}
	}
	stateTime += elapsedTime;
}

void Turtle::cleanHost() {
	leftWalking->cleanHost();
	rightWalking->cleanHost();
	delete leftWalking;
	delete rightWalking;
	SDL_FreeSurface(shellOne);
	SDL_FreeSurface(shellTwo);

	if (flyingLeft != NULL) {
		flyingLeft->cleanHost();
		flyingRight->cleanHost();
		delete flyingLeft;
		delete flyingRight;
	}
}

void Turtle::handleCollision(Player *player, int side) {
	switch (state) {
	case STATE_NORMAL:
		if (side == TOP) {
			setState(STATE_SHELL);
			player->setY(getY() - player->getHeight());
			player->jump(true);
		} else
			player->hurt();
		break;
	case STATE_SHELL:

		if (side == TOP) {
			player->setY(getY() - player->getHeight());
			player->jump(true);
		}
		if (player->getX() > getX() + (float) getWidth() / 2.0) {
			playerCollSide = RIGHT;
			if (side != TOP)
				player->setX(getX() + getWidth());
		} else {
			playerCollSide = LEFT;
			if (side != TOP)
				player->setX(getX() - player->getWidth());
		}
		setState(STATE_MOVING_SHELL);
		break;
	case STATE_MOVING_SHELL:
		if (side == TOP) {
			player->setY(getY() - player->getHeight());
			player->jump(true);
			setState(STATE_SHELL);
		} else {
			player->hurt();
		}
		break;
	case STATE_FLYING:
		if (side == TOP) {
			player->setY(getY() - player->getHeight());
			player->jump(true);
			setState(STATE_NORMAL);
		} else
			player->hurt();

		break;
	}

}

void Turtle::setState(int state) {
	if (this->state == state)
		return;

	this->state = state;
	stateTime = 0;

	switch (state) {
	case STATE_NORMAL:
		setVelocityX(getMaxSpeed());
		setVelocityY(0);
		break;
	case STATE_SHELL:
		image = shellOne;
		setVelocityX(0.0);
		break;
	case STATE_MOVING_SHELL:
		image = shellOne;
		if (playerCollSide == RIGHT) {
			setVelocityX(-0.5);
		} else if (playerCollSide == LEFT) {
			setVelocityX(0.5);
		}
		break;
	}
}

void Turtle::collideVertical() {
	if (state == STATE_FLYING) {
		float y = getVelocityY() < 0.0 ? 0.5 : -0.5;
		setVelocityY(y);
	} else
		setVelocityY(0.0);
}
