/*
 * Math.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef MATH_H_
#define MATH_H_

class Math {
public:
	static int max(int  a, int  b) {
		if (a > b)
			return a;
		return b;
	}

	static int  min(int  a, int  b) {
		if (a < b)
			return a;
		return b;
	}

	static int round(float f) {
		return static_cast<int> (f + 0.5f);
	}

	static float max(float a, float b) {
		if (a > b)
			return a;
		return b;
	}

	static float min(float a, float b) {
		if (a < b)
			return a;
		return b;
	}
};

#endif /* MATH_H_ */
