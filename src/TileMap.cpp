/*
 * TileMap.cpp
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#include "TileMap.h"
#include "TileMapRenderer.h"

TileMap::TileMap(int width, int height) :
	width(width), height(height) {
	tiles = new Tile**[width];
	for (int i = 0; i < width; i++) {
		tiles[i] = new Tile*[height];

		for (int j = 0; j < height; j++)
			tiles[i][j] = NULL;
	}
	player = NULL;
	background = NULL;
	sprites = new SpriteList;
	brokenBricksList = new BrokenBricksList;
	pipeEntryList = NULL;
	nextLevelEntry = NULL;
}

TileMap::~TileMap() {
	for (int i = 0; i < width; i++) {
		// TODO: (?) kasowanie pojedynczych Tile
		// for(int j = 0; j < height; j++)
		// 	   delete tiles[i][j];
		delete[] tiles[i];
	}
	delete[] tiles;

	for (SpriteListItor it = sprites->begin(); it != sprites->end(); ++it) {
		delete (*it);
	}

	delete player;
	delete sprites;

	for (BrokenBricksListIter iter = brokenBricksList->begin(); iter
			!= brokenBricksList->end(); iter++) {
		delete (*iter);
	}
	delete brokenBricksList;

	SDL_FreeSurface(background);

	if (pipeEntryList != NULL) {
		for (PipeEntryListIter iter = pipeEntryList->begin(); iter
				!= pipeEntryList->end(); iter++) {
			delete (*iter);

		}
		delete pipeEntryList;
	}

	if (nextLevelEntry != NULL)
		delete nextLevelEntry;
}

Tile* TileMap::getTile(int x, int y) {
	//assert(x >= 0 && y >= 0 && "x lub y mniejsze od 0 :P!");
	if (x < 0 || y < 0 || x >= getWidth() || y >= getHeight()) {
		return NULL;
	} else {
		return tiles[x][y];
	}
}

void TileMap::update(int elapsedTime) {
	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			if (tiles[i][j] != NULL)
				tiles[i][j]->update(elapsedTime);
		}
	}

	BrokenBricksListIter iter = brokenBricksList->begin();
	while (iter != brokenBricksList->end()) {
		if ((*iter)->isExpired()) {
			delete (*iter);
			iter = brokenBricksList->erase(iter);
			continue;
		}
		(*iter)->update(elapsedTime);
		iter++;
	}
}

void TileMap::removePipeEntry(PipeEntry* pipeEntry) {
	pipeEntryList->remove(pipeEntry);
	//PipeEntryListIter iter = pipeEntryList->begin();
	//	while(iter != pipeEntryList->end()) {
	//		if((*iter) == pipeEntry) {
	//			delete pipeEntry;
	//			pipeEntryList->
	//			break;
	//		}
	// 	i++
	//	}
}
