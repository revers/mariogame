/*
 * Creature.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef CREATURE_H_
#define CREATURE_H_

#include "Animation.h"
#include "Sprite.h"
//#include "Player.h"

class Player;

class Creature: public Sprite {
protected:
	static const int DIE_TIME = 1000;
	Animation *leftWalking;
	Animation *rightWalking;
	SDL_Surface *deadImage;
	int state;
	int stateTime;
	//virtual void hurt();
public:
	static const int STATE_DEAD = 0;
	static const int STATE_DYING = 1;
	static const int STATE_NORMAL = 2;

//	static const int REACTION_NOTHING = 0;
//	static const int REACTION_ENEMY_HURT = 1;
//	static const int REACTION_PLAYER_HURT = 2;

	Creature(Animation *left, Animation *right, SDL_Surface* deadImage);

	virtual void wakeUp();
	virtual void update(int elapsedTime);
	virtual void setState(int state);

	//virtual int getPlayerCollisionReaction(int side);

	virtual ~Creature();
	virtual Sprite* clone() = 0;
	virtual void handleCollision(Player *player, int side);
	virtual void cleanHost();

	virtual float getMaxSpeed();
	int getState();
	bool isAlive();

	virtual inline bool isExpired();
	virtual inline void setExpired(bool expired);

};

inline bool Creature::isExpired() {
	return (expired == true) || (state == STATE_DEAD);
}

inline void Creature::setExpired(bool expired) {
	this->expired = expired;
	if (expired)
		state = STATE_DEAD;
}

#endif /* CREATURE_H_ */
