/*
 * InputManager.cpp
 *
 *  Created on: 2009-11-30
 *      Author: Revers
 */

#include "InputManager.h"

InputManager::InputManager() {
	for (int i = 0; i < NUM_KEY_CODES; i++)
		keyActions[i] = NULL;

}

InputManager::~InputManager() {
}

void InputManager::mapToKey(GameAction *gameAction, int keyCode) {
	keyActions[keyCode] = gameAction;
}

void InputManager::clearMap(GameAction* gameAction) {
	for (int i = 0; i < NUM_KEY_CODES; i++) {
		if (keyActions[i] == gameAction) {
			keyActions[i] = NULL;
		}
	}
	gameAction->reset();
}

GameAction* InputManager::getKeyAction(SDL_Event &e) {
	int keyCode = e.key.keysym.sym;
	if (keyCode < NUM_KEY_CODES) {
		return keyActions[keyCode];
	} else {
		return NULL;
	}
}

void InputManager::keyPressed(SDL_Event &e) {
	GameAction* gameAction = getKeyAction(e);
	if (gameAction != NULL) {
		gameAction->press();
	}
}

void InputManager::keyReleased(SDL_Event &e) {
	GameAction *gameAction = getKeyAction(e);
	if (gameAction != NULL) {
		gameAction->release();
	}
}

void InputManager::handleEvent(SDL_Event &e) {
	if (e.type == SDL_KEYDOWN)
		keyPressed(e);
	else if (e.type == SDL_KEYUP)
		keyReleased(e);
}
