/*
 * InputManager.h
 *
 *  Created on: 2009-11-30
 *      Author: Revers
 */

#ifndef INPUTMANAGER_H_
#define INPUTMANAGER_H_

#include "SDL/SDL.h"
#include "GameAction.h"

class InputManager {
	static const int NUM_KEY_CODES = 600;
	GameAction* keyActions[NUM_KEY_CODES];
public:
	InputManager();
	virtual ~InputManager();

	void mapToKey(GameAction *gameAction, int keyCode);
	void clearMap(GameAction* gameAction);
	GameAction* getKeyAction(SDL_Event &e);
	void keyPressed(SDL_Event &e);
	void keyReleased(SDL_Event &e);
	void handleEvent(SDL_Event &e);
};

#endif /* INPUTMANAGER_H_ */
