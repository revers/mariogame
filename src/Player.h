/*
 * Player.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include "Creature.h"

struct PlayerAnimations {
	SDL_Surface* left;
	SDL_Surface* right;
	SDL_Surface* jumpLeft;
	SDL_Surface* jumpRight;
	SDL_Surface* dead;

	Animation* leftWalking;
	Animation* rightWalking;
};

struct PlayerData {
	int coins;
	PlayerAnimations *smallAnim;
	PlayerAnimations *superAnim;
	PlayerAnimations *fieryAnim;

	PlayerAnimations *currentAnim;

	int lives;

	PlayerData() {
		coins = 0;
		currentAnim = NULL;
		lives = 3;
	}
};

class Player: public Creature {
	static const float JUMP_SPEED = -.75f;
	static const int HURT_TIME = 1000;
	static const int ENTRANCE_TIME = 950;
	bool onGround;
	PlayerData *playerData;
	int direction;
	int width;
	int height;
	SDL_Surface* oldImage;
	int oldState;
	bool ignoringControls;
	int entranceSide;
	int exitEndY;

	void cleanHostAnimations(PlayerAnimations *pAnim);
public:
	static const int STATE_SUPER = 3;
	static const int STATE_FIERY = 4;
	static const int STATE_HURT = 5;
	static const int STATE_ENTERING = 6;
	static const int STATE_ENTERED = 7;
	static const int STATE_EXITING = 8;

	Player(PlayerData* playerData);
	virtual ~Player();
	virtual inline float getMaxSpeed();
	virtual inline Sprite* clone();
	virtual void cleanHost();
	virtual void update(int elapsedTime);
	virtual void setState(int state);

	virtual void setY(float y);
	void collideVertical();
	void jump(bool forceJump);
	void updatePosition(int elapsedTime);

	inline void collideHorizontal();
	inline void setCoins(int coins);
	inline int getCoins();
	inline void increaseCoins();
	inline int getDirection();

	inline void hurt();

	inline bool isIgnoringControls();
	inline void setEntranceSide(int side);
	inline int getLives();
	inline void setLives(int lives);
	inline void increaseLives();
	inline void decreaseLives();

	inline void wakeUpTest() {
	}

	virtual int getHeight();
	virtual int getWidth();
};

inline void Player::hurt() {
	setState(STATE_HURT);
}

inline void Player::setEntranceSide(int side) {
	this->entranceSide = side;
}

inline int Player::getDirection() {
	return direction;
}

inline void Player::setCoins(int coins) {
	playerData->coins = coins;
}

inline int Player::getCoins() {
	return playerData->coins;
}

inline void Player::increaseCoins() {
	playerData->coins++;
	if(playerData->coins >= 100) {
		playerData->coins = 0;
		increaseLives();
	}
}

inline float Player::getMaxSpeed() {
	return 0.45;
}

inline void Player::collideHorizontal() {
	setVelocityX(0);
}

inline Sprite* Player::clone() {
	return new Player(playerData);
}

bool Player::isIgnoringControls() {
	return ignoringControls;
}

inline int Player::getLives() {
	return playerData->lives;
}
inline void Player::setLives(int lives) {
	this->playerData->lives = lives;
}
inline void Player::increaseLives() {
	playerData->lives++;
}
inline void Player::decreaseLives() {
	playerData->lives--;
}

#endif /* PLAYER_H_ */
