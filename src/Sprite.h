/*
 * Sprite.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef SPRITE_H_
#define SPRITE_H_

#include "SDL/SDL.h"
#include "Animation.h"
#include "Math.h"

class Sprite {
protected:
	Animation *anim;
	SDL_Surface *image;
	bool expired;
	bool movable;
	float x;
	float y;
	float dx;
	float dy;

public:
	//	static const int SIDE_LEFT = 0;
	//	static const int SIDE_RIGHT = 1;
	//	static const int SIDE_TOP = 2;
	//	static const int SIDE_BOTTOM = 3;

	Sprite(Animation *anim);
	virtual ~Sprite();
	virtual Sprite* clone() = 0;
	virtual void cleanHost() = 0;
	virtual void update(int elapsedTime);

	virtual inline bool isExpired();
	virtual inline bool isMovable();
	virtual inline void setExpired(bool expired);
	virtual bool isFlying();
	virtual void collideHorizontal();
	virtual void collideVertical();

	inline float getX();
	virtual void setX(float x);
	inline float getY();
	virtual void setY(float y);
	inline float getVelocityX();
	virtual void setVelocityX(float dx);
	inline float getVelocityY();
	virtual void setVelocityY(float dy);
	inline SDL_Surface* getImage();
	virtual inline int getHeight();
	virtual inline int getWidth();

};

inline void Sprite::setExpired(bool expired) {
	this->expired = expired;
}

inline bool Sprite::isExpired() {
	return expired;
}

inline bool Sprite::isMovable() {
	return movable;
}
inline float Sprite::getX() {
	return x;
}

inline float Sprite::getY() {
	return y;
}

inline float Sprite::getVelocityX() {
	return dx;
}

inline float Sprite::getVelocityY() {
	return dy;
}

inline SDL_Surface* Sprite::getImage() {
	return image;
}

inline int Sprite::getHeight() {
	return image->h;
}
inline int Sprite::getWidth() {
	return image->w;
}

//inline Sprite* Sprite::clone() {
//	return new Sprite(anim);
//}

#endif /* SPRITE_H_ */
