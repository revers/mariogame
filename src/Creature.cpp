/*
 * Creature.cpp
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#include "Creature.h"
#include "Constants.h"

#include "Player.h"

#include <iostream>
using namespace std;

Creature::Creature(Animation *left, Animation *right, SDL_Surface* deadImage) :
	Sprite(right) {
	this->leftWalking = left;
	this->rightWalking = right;
	this->deadImage = deadImage;
	state = STATE_NORMAL;

}

Creature::~Creature() {
//	cout << "Destruktor creature" << endl;
	//delete leftWalking;
	//	delete rightWalking;
	//delete deadImage;
	//delete deadLeft;
	//delete deadRight;
}

void Creature::cleanHost() {
	leftWalking->cleanHost();
	rightWalking->cleanHost();
	SDL_FreeSurface(deadImage);
	//deadLeft->cleanHost();
	//deadRight->cleanHost();
}
void Creature::wakeUp() {
	if (getState() == STATE_NORMAL && getVelocityX() == 0) {
		setVelocityX(-getMaxSpeed());
	}
}

void Creature::update(int elapsedTime) {

	if (state == STATE_DYING) {
		image = deadImage;
	} else {
		Animation *newAnim = anim;
		if (getVelocityX() < 0) {
			newAnim = leftWalking;
		} else if (getVelocityX() > 0) {
			newAnim = rightWalking;
		}

		if (anim != newAnim) {
			anim = newAnim;
			anim->start();
		} else {
			anim->update(elapsedTime);
		}

		image = anim->getImage();
	}

	stateTime += elapsedTime;
	if (state == STATE_DYING && stateTime >= DIE_TIME) {
		setState(STATE_DEAD);
	}
}

void Creature::setState(int state) {
	if (this->state != state) {
		this->state = state;
		stateTime = 0;
		if (state == STATE_DYING) {
			setVelocityX(0);
			setVelocityY(0);
		}
	}
}

bool Creature::isAlive() {
	return (state > STATE_DYING);
}

float Creature::getMaxSpeed() {
	return 0;
}

int Creature::getState() {
	return state;
}

//int Creature::getPlayerCollisionReaction(int side) {
//	if (side == TOP) {
//		setState(STATE_DYING);
//		return REACTION_ENEMY_HURT;
//	} else
//		return REACTION_PLAYER_HURT;
//}

void Creature::handleCollision(Player *player, int side) {
	if (side == TOP) {
		player->setY(getY() - player->getHeight());
		player->jump(true);
		setState(STATE_DYING);
	} else {
		player->hurt();
	}
}

