/*
 * QuestionMarkTile.h
 *
 *  Created on: 2009-12-06
 *      Author: Revers
 */

#ifndef QUESTIONMARKTILE_H_
#define QUESTIONMARKTILE_H_
#include "Tile.h"
#include "Animation.h"
#include <cassert>

class QuestionMarkTile: public Tile {
	Animation* anim;
	SDL_Surface* emptyBox;
	int spriteID;
	int quantity;
public:
	QuestionMarkTile(Animation *anim, SDL_Surface* emptyBox, int spriteID,
			int quantity);
	virtual ~QuestionMarkTile();

	virtual void update(int elapsedTime);
	virtual Tile* clone();
	virtual void cleanHost();

	inline int getSpriteID();
	inline void setSpriteID(int spriteID);
	inline int getQuantity();
	inline void setQuantity(int quantity);
	inline void decreaseQuantity();
};

inline void QuestionMarkTile::setSpriteID(int spriteID) {
	this->spriteID = spriteID;
}

inline void QuestionMarkTile::setQuantity(int quantity) {
	this->quantity = quantity;
}

inline int QuestionMarkTile::getSpriteID() {
	return spriteID;
}

inline int QuestionMarkTile::getQuantity() {
	return quantity;
}

inline void QuestionMarkTile::decreaseQuantity() {
	quantity--;
	assert(quantity >= 0 && "QestionMarkTile::decreaseQuantity()");
}

#endif /* QUESTIONMARKTILE_H_ */
