/*
 * Sprite.cpp
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#include "Sprite.h"

Sprite::Sprite(Animation *anim) :
	anim(anim) {
	x = 0;
	y = 0;
	dx = 0.0f;
	dy = 0.0f;
	movable = true;
	expired = false;
	if (anim != NULL)
		image = anim->getImage();
	else
		image = NULL;
}

Sprite::~Sprite() {
	//delete anim;
}

void Sprite::update(int elapsedTime) {
	//x += dx * elapsedTime;
	//y += dy * elapsedTime;
	anim->update(elapsedTime);
	image = anim->getImage();
}

void Sprite::setY(float y) {
	this->y = y;
}

void Sprite::setX(float x) {
	this->x = x;
}

void Sprite::setVelocityX(float dx) {
	this->dx = dx;
}

void Sprite::setVelocityY(float dy) {
	this->dy = dy;
}

void Sprite::collideHorizontal() {
	setVelocityX(-getVelocityX());
}

void Sprite::collideVertical() {
	setVelocityY(0);
}
bool Sprite::isFlying() {
	return false;
}
