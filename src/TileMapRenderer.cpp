/*
 * TileMapRenderer.cpp
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#include "TileMapRenderer.h"
#include <iostream>
using namespace std;

void TileMapRenderer::applySurface(int x, int y, SDL_Surface* source,
		SDL_Surface* destination) {
	SDL_Rect offset;

	offset.x = x;
	offset.y = y;
	SDL_BlitSurface(source, NULL, destination, &offset);
}

TileMapRenderer::TileMapRenderer() {

}

TileMapRenderer::~TileMapRenderer() {
}

void TileMapRenderer::draw(SDL_Surface* screen, TileMap *map, int screenWidth,
		int screenHeight, int elapsedTime) {
	Sprite *player = map->getPlayer();
	int mapWidth = tilesToPixels(map->getWidth());

	int offsetX = screenWidth / 2 - Math::round((player->getX())) - TILE_SIZE;
	offsetX = Math::min(offsetX, 0);
	offsetX = Math::max(offsetX, screenWidth - mapWidth);

	int offsetY = screenHeight - tilesToPixels(map->getHeight());

	// rysuj czarne tlo:
	if (map->getBackground() == NULL || screenHeight > map->getBackground()->h) {
		SDL_Rect rect = { 0, 0, screenWidth, screenHeight };
		SDL_FillRect(screen, &rect, 0);
	}

	// rysuj tlo:
	if (map->getBackground() != NULL) {
		int x = offsetX * (screenWidth - map->getBackground()->w)
				/ (screenWidth - mapWidth);
		int y = screenHeight - map->getBackground()->h;
		applySurface(x, y, map->getBackground(), screen);
	}

	// rysuj player'a:
	applySurface(Math::round(player->getX()) + offsetX, Math::round(
			player->getY()) + offsetY, player->getImage(), screen);

	// rysuj sprite'y:
	SpriteListItor i = map->getBeginSprite();
	SpriteListItor lastItor = map->getEndSprite();
	for (; i != lastItor; ++i) {
		Sprite *sprite = (*i);
		int x = Math::round(sprite->getX()) + offsetX;
		int y = Math::round(sprite->getY()) + offsetY;
		if (sprite->getImage() != NULL)
			applySurface(x, y, sprite->getImage(), screen);

		// obudz creature'a gdy po raz pierwszy pojawia sie na ekranie:
		Creature *creature = dynamic_cast<Creature*> (sprite);
		if (creature != NULL && x >= 0 && x < screenWidth) {
			creature->wakeUp();
		}
	}

	// Rysuj widzialne kafelki:
	int firstTileX = pixelsToTiles(-offsetX);
	int lastTileX = firstTileX + pixelsToTiles(screenWidth) + 1;
	for (int y = 0; y < map->getHeight(); y++) {
		for (int x = firstTileX; x <= lastTileX; x++) {
			Tile* tile = map->getTile(x, y);
			if (tile != NULL && tile->isVisible()) {
				if (tile->isDestroyed()) {
					delete tile;
					map->setTile(x, y, NULL);
				} else {
					applySurface(tilesToPixels(x) + offsetX, tilesToPixels(y)
							+ offsetY, tile->getImage(), screen);
				}
			}
		}
	}

	BrokenBricksList *list = map->getBrokenBricksList();

	for (BrokenBricksListIter iter = list->begin(); iter != list->end(); iter++) {
		BrokenBricks *bricks = *iter;

		Sprite *s = bricks->getOne();
		int x = Math::round(s->getX()) + offsetX;
		int y = Math::round(s->getY()) + offsetY;
		applySurface(x, y, s->getImage(), screen);

		s = bricks->getTwo();
		x = Math::round(s->getX()) + offsetX;
		y = Math::round(s->getY()) + offsetY;
		applySurface(x, y, s->getImage(), screen);

		s = bricks->getThree();
		x = Math::round(s->getX()) + offsetX;
		y = Math::round(s->getY()) + offsetY;
		applySurface(x, y, s->getImage(), screen);

		s = bricks->getFour();
		x = Math::round(s->getX()) + offsetX;
		y = Math::round(s->getY()) + offsetY;
		applySurface(x, y, s->getImage(), screen);
	}
}

