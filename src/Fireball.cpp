/*
 * Fireball.cpp
 *
 *  Created on: 2009-12-08
 *      Author: Revers
 */

#include "Fireball.h"
#include "Constants.h"

Fireball::Fireball(Animation *anim) :
	Sprite(anim) {
	setVelocityX(0.6);
	setVelocityY(0.4);
	durationTime = 0;
	direction = RIGHT;
}

Fireball::~Fireball() {
}

void Fireball::collideVertical() {
	float vel = (getVelocityY() / getVelocityY()) * (-0.4);
	setVelocityY(vel);
}

void Fireball::update(int elapsedTime) {
	Sprite::update(elapsedTime);
	durationTime += elapsedTime;
	if (durationTime >= LIFE_TIME) {
		expired = true;
	}
}
