/*
 * GameAction.cpp
 *
 *  Created on: 2009-11-30
 *      Author: Revers
 */

#include "GameAction.h"

void GameAction::press(int  amount) {
	if (state != STATE_WAITING_FOR_RELEASE) {
		this->amount += amount;
		state = STATE_PRESSED;
	}
}

int  GameAction::getAmount() {
	int  retVal = amount;
	if (retVal != 0) {
		if (state == STATE_RELEASED) {
			amount = 0;
		} else if (behavior == DETECT_INITAL_PRESS_ONLY) {
			state = STATE_WAITING_FOR_RELEASE;
			amount = 0;
		}
	}
	return retVal;
}
