/*
 * Grub.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef GRUB_H_
#define GRUB_H_

#include "Creature.h"

class EvilMashroom: public Creature {
public:
	inline EvilMashroom(Animation *left, Animation *right, SDL_Surface* deadImage);
	virtual float getMaxSpeed();
	inline Creature* clone();
	virtual inline ~EvilMashroom();
};

inline EvilMashroom::EvilMashroom(Animation *left, Animation *right, SDL_Surface* deadImage) :
	Creature(left, right, deadImage) {
}

inline EvilMashroom::~EvilMashroom() {
	delete leftWalking;
	delete rightWalking;
}

inline Creature* EvilMashroom::clone() {
//	std::cout << "grub.clone()" << std::endl;
	return new EvilMashroom(leftWalking->clone(), rightWalking->clone(), deadImage);
}

float EvilMashroom::getMaxSpeed() {
	return 0.1f;
}

#endif /* GRUB_H_ */
