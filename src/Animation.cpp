/*
 * Animation.cpp
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#include "Animation.h"

//using namespace std;
using std::vector;

Animation::Animation() {
	frames = new vector<AnimFrame*>;
	totalDuration = 0;
	start();
}

Animation::Animation(vector<AnimFrame*> *frames, int totalDuration) :
	frames(frames), totalDuration(totalDuration) {
	start();
}

Animation::~Animation() {
	//std::cout << "Destruktor klasy animacja..." << std::endl;
	//delete frames;
}

void Animation::start() {
	animTime = 0;
	currFrameIndex = 0;
}

void Animation::addFrame(SDL_Surface *image, int duration) {
	totalDuration += duration;
	frames->push_back(new AnimFrame(image, totalDuration));
}

void Animation::update(int elapsedTime) {
	if (frames->size() > 1) {
		animTime += elapsedTime;

		if (animTime >= totalDuration) {
			animTime = animTime % totalDuration;
			//animTime = 0;
			currFrameIndex = 0;
		}

		while (animTime > (*frames)[currFrameIndex]->endTime) {
			currFrameIndex++;
		}
	}
}

SDL_Surface* Animation::getImage() {
	if (frames->size() == 0) {
		return NULL;
	} else {
		return (*frames)[currFrameIndex]->image;
	}
}

Animation* Animation::clone() {
	//std::cout << "animation.clone()" << std::endl;
	return new Animation(frames, totalDuration);
}

void Animation::addUniqSurface(vector<SDL_Surface*> &vect, SDL_Surface *surface) {
	bool isNotIn = true;
	for(unsigned i = 0; i < vect.size(); i++) {
		if(vect[i] == surface) {
			isNotIn = false;
			break;
		}
	}
	if(isNotIn) {
		vect.push_back(surface);
	}
}

void Animation::cleanHost() {
	//std::cout << "cleanHost in ANimation" << std::endl;
	vector<SDL_Surface*> uniq;

	for(int i = 0; i < (int)frames->size(); i++) {
		addUniqSurface(uniq, (*frames)[i]->image);
		delete (*frames)[i];
	}
	delete frames;
	frames = NULL;

	for(unsigned i = 0; i < uniq.size(); i++) {
		SDL_FreeSurface(uniq[i]);
	}
}
