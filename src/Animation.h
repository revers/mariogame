/*
 * Animation.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef ANIMATION_H_
#define ANIMATION_H_

#include <vector>
#include "SDL/SDL.h"
#include <iostream>


class Animation {
private:
	struct AnimFrame {
		SDL_Surface* image;
		int endTime;
		AnimFrame(SDL_Surface* image, int endTime) {
			this->image = image;
			this->endTime = endTime;
		}
	};

	std::vector<AnimFrame*> *frames;
	int currFrameIndex;
	int animTime;
	int totalDuration;
	Animation(std::vector<AnimFrame*> *frames, int totalDuration);
	void addUniqSurface(std::vector<SDL_Surface*> &vect, SDL_Surface* surface);
public:
	Animation();
	virtual ~Animation();

	void addFrame(SDL_Surface *image, int duration);
	void start();
	void update(int elapsedTime);
	SDL_Surface* getImage();
	Animation* clone();

	void cleanHost();

	inline int getAnimTime();
};

inline int Animation::getAnimTime() {
	return animTime;
}

#endif /* ANIMATION_H_ */
