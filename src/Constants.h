/*
 * Constants.h
 *
 *  Created on: 2009-12-08
 *      Author: Revers
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define SCREEN_BPP 32
#define FRAMES_PER_SECOND 60
#define GRAVITY 0.002f
#define TILE_SIZE 40

#define LEFT 0
#define RIGHT 1
#define TOP 2
#define BOTTOM 3

#endif /* CONSTANTS_H_ */
