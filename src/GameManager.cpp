/*
 * GameManager.cpp
 *
 *  Created on: 2009-11-30
 *      Author: Revers
 */

#include <sstream>
#include "GameManager.h"
#include "QuestionMarkTile.h"
#include "BricksTile.h"
#include "Constants.h"
#include "Fireball.h"
#include "PipeEntranceTile.h"
#include "Plant.h"
#include "Turtle.h"

GameManager::~GameManager() {
	if (map != NULL)
		delete map;
	if (screen != NULL)
		SDL_FreeSurface(screen);
	if (textImage != NULL)
		SDL_FreeSurface(textImage);
	TTF_CloseFont(normalFont);
	TTF_CloseFont(bigFont);

	delete collisionTiles;
}

GameManager::GameManager(SDL_Surface *screen) :
	screen(screen), moveLeft("moveLeft"), moveRight("moveRight"), jump("jump",
			GameAction::DETECT_INITAL_PRESS_ONLY), exit("exit",
			GameAction::DETECT_INITAL_PRESS_ONLY), shoot("shoot",
			GameAction::DETECT_INITAL_PRESS_ONLY), moveDown("moveDown",
			GameAction::DETECT_INITAL_PRESS_ONLY) {

	inputMgr.mapToKey(&moveLeft, static_cast<int> (SDLK_LEFT));
	inputMgr.mapToKey(&moveRight, static_cast<int> (SDLK_RIGHT));
	inputMgr.mapToKey(&jump, static_cast<int> (SDLK_SPACE));
	inputMgr.mapToKey(&exit, static_cast<int> (SDLK_ESCAPE));
	inputMgr.mapToKey(&shoot, static_cast<int> (SDLK_RCTRL));
	inputMgr.mapToKey(&moveDown, static_cast<int> (SDLK_DOWN));

//	 pause("pause",
//				GameAction::DETECT_INITAL_PRESS_ONLY), enter("enter",
//				GameAction::DETECT_INITAL_PRESS_ONLY)
//	inputMgr.mapToKey(&pause, static_cast<int> (SDLK_p));
//	inputMgr.mapToKey(&enter, static_cast<int> (SDLK_RETURN));

	map = resourceManager.loadNextMap();
	isRunning = true;
	mapChanged = false;
	collisionTiles = new CollisionTiles;
	textImage = NULL;

	isSubmap = false;
	oldMap = NULL;
}

void GameManager::run() {
	if (init()) {
		gameLoop();
	} else {
		std::cout << "INIT NIEUDANY :(" << std::endl;
	}
}

bool GameManager::init() {
	if (TTF_Init() == -1)
		return false;

	normalFont = TTF_OpenFont("DejaVuSansBoldOblique.ttf", 16);
	if (normalFont == NULL)
		return false;

	bigFont = TTF_OpenFont("DejaVuSansBoldOblique.ttf", 28);
	if (bigFont == NULL)
		return false;

	SDL_WM_SetCaption(TITLE, NULL);
	return true;
}

void GameManager::gameLoop() {
	int frame = 0;
	Timer fps;
	Timer fps_counter;
	Timer update_counter;
	SDL_Color white = { 255, 255, 255 };
	SDL_Color black = { 0, 0, 0 };
	SDL_Color red = { 255, 0, 0 };

	update_counter.start();
	fps_counter.start();
	int currTime = SDL_GetTicks();

	while (isRunning) {
		fps.start();

		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				isRunning = false;
			}
			inputMgr.handleEvent(event);
		}

		int elapsedTime = SDL_GetTicks() - currTime;
		currTime += elapsedTime;

		int lives = map->getPlayer()->getLives();
		if (lives >= 0)
			update(elapsedTime);

		draw(elapsedTime);

		stringstream sout;
		sout << "coins x " << map->getPlayer()->getCoins();
		drawString(sout.str(), 10, 10, normalFont, &white, &black);

		sout.str("");
		sout << "level " << resourceManager.getCurrentMap();
		drawString(sout.str(), 280, 10, normalFont, &white, &black);

		for (int i = 0; i < map->getPlayer()->getLives(); i++) {
			TileMapRenderer::applySurface(SCREEN_WIDTH - 40 - i * 20, 15,
					resourceManager.getHeartImage(), screen);
		}

		if (lives < 0)
			drawString("Game Over", 235, 200, bigFont, &red, &black);

		if (SDL_Flip(screen) == -1) {
			return;
		}

		frame++;

		if ((fps.get_ticks() < 1000 / FRAMES_PER_SECOND)) {
			SDL_Delay((1000 / FRAMES_PER_SECOND) - fps.get_ticks());
		}

		//=========================================================
		if (update_counter.get_ticks() > 1000) {
			std::ostringstream caption;
			caption << TITLE << frame / (fps_counter.get_ticks() / 1000.f);

			SDL_WM_SetCaption(caption.str().c_str(), NULL);

			update_counter.start();
		}
		//=========================================================
	}
}

void GameManager::drawString(std::string str, int x, int y, TTF_Font *font,
		SDL_Color* color1, SDL_Color* color2) {
	const char* c_str = str.c_str();
	if (textImage = TTF_RenderText_Solid(font, c_str, *color2)) {
		TileMapRenderer::applySurface(x - 1, y - 1, textImage, screen);
		TileMapRenderer::applySurface(x + 1, y + 1, textImage, screen);
		TileMapRenderer::applySurface(x - 1, y + 1, textImage, screen);
		TileMapRenderer::applySurface(x + 1, y - 1, textImage, screen);
		SDL_FreeSurface(textImage);
	}
	if (textImage = TTF_RenderText_Solid(font, c_str, *color1)) {
		TileMapRenderer::applySurface(x, y, textImage, screen);
		SDL_FreeSurface(textImage);
	}
}

void GameManager::checkInput(int elapsedTime) {

	if (exit.isPressed()) {
		exit(0);
	}

	Player *player = map->getPlayer();
	if (player->isAlive() && player->isIgnoringControls() == false) {
		float velocityX = 0;
		if (moveLeft.isPressed()) {
			velocityX -= player->getMaxSpeed();
		}
		if (moveRight.isPressed()) {
			velocityX += player->getMaxSpeed();
		}
		if (jump.isPressed()) {
			//cout << "Jump is pressed" << endl;
			player->jump(false);
		}
		player->setVelocityX(velocityX);

		if (player->getState() == Player::STATE_FIERY && shoot.isPressed()) {
			Sprite* fireball = resourceManager.getAdditionalSprite(
					ResourceManager::SPRITE_FIREBALL);
			float x;
			float y = player->getY() + (float) player->getHeight() / 2.0f;
			if (player->getDirection() == RIGHT) {
				x = player->getX() + (float) player->getWidth();
			} else {
				x = player->getX();
				fireball->setVelocityX(-0.6);
			}
			fireball->setX(x);
			fireball->setY(y);
			map->addSprite(fireball);
		} else
			shoot.reset();

	}

}

void GameManager::draw(int elapsedTime) {
	renderer.draw(screen, map, SCREEN_WIDTH, SCREEN_HEIGHT, elapsedTime);//(g, map,
}

CollisionTiles* GameManager::getTileCollision(Sprite *sprite, float newX,
		float newY) {
	collisionTiles->clean();

	float fromX = Math::min(sprite->getX(), newX);
	float fromY = Math::min(sprite->getY(), newY);
	float toX = Math::max(sprite->getX(), newX);
	float toY = Math::max(sprite->getY(), newY);
	if (toX < 0.0f)
		toX = 0.0f;

	int fromTileX = TileMapRenderer::pixelsToTiles(fromX);
	int fromTileY = TileMapRenderer::pixelsToTiles(fromY);
	int toTileX = TileMapRenderer::pixelsToTiles(toX + sprite->getWidth() - 1);
	int toTileY = TileMapRenderer::pixelsToTiles(toY + sprite->getHeight() - 1);

	for (int x = fromTileX; x <= toTileX; x++) {
		for (int y = fromTileY; y <= toTileY; y++) {
			if (x < 0 || x >= map->getWidth() || y > map->getHeight()) {
				collisionTiles->addDummyTile(x, y);
			} else if (map->getTile(x, y) != NULL) {
				Tile *tile = map->getTile(x, y);
				tile->setX(x);
				tile->setY(y);
				collisionTiles->addTile(tile);
			}
		}
	}
	return collisionTiles;
}

bool GameManager::isCollision(Sprite *s1, Sprite *s2) {
	if (s1 == s2) {
		return false;
	}

	Creature* cre = dynamic_cast<Creature*> (s1);
	if (cre != NULL && ((Creature*) s1)->isAlive() == false) {
		return false;

	}
	cre = dynamic_cast<Creature*> (s2);
	if (cre != NULL && ((Creature*) s2)->isAlive() == false) {
		return false;
	}

	int s1x = Math::round(s1->getX());
	int s1y = Math::round(s1->getY());
	int s2x = Math::round(s2->getX());
	int s2y = Math::round(s2->getY());

	return (s1x < s2x + s2->getWidth() && s2x < s1x + s1->getWidth() && s1y
			< s2y + s2->getHeight() && s2y < s1y + s1->getHeight());
}

Sprite* GameManager::getSpriteCollision(Sprite *sprite) {

	SpriteListItor i = map->getBeginSprite();
	SpriteListItor lastItor = map->getEndSprite();
	for (; i != lastItor; ++i) {
		Sprite *otherSprite = (Sprite*) (*i);
		if (isCollision(sprite, otherSprite)) {
			return otherSprite;
		}
	}

	return NULL;
}

void GameManager::update(int elapsedTime) {

	Creature *player = map->getPlayer();

	if (player->getState() == Creature::STATE_DEAD) {
		player->setState(Player::STATE_NORMAL);
		delete map;
		map = resourceManager.reloadMap();
		resetButtons();
		if (isSubmap)
			delete oldMap;
		return;
	}

	checkInput(elapsedTime);
	updateMovableSprite(player, elapsedTime);
	if (mapChanged) {
		mapChanged = false;
		return;
	}
	moveDown.reset();

	player->update(elapsedTime);

	SpriteListItor iter = map->getBeginSprite();
	SpriteListItor lastItor = map->getEndSprite();

	while (iter != lastItor) {
		Sprite *sprite = (Sprite*) (*iter);
		if (sprite->isExpired() == true) {
			iter = map->eraseSpriteIter(iter);
			continue;
		} else if (sprite->isMovable()) {
			updateMovableSprite(sprite, elapsedTime);
			if (mapChanged) {
				mapChanged = false;
				return;
			}
		}
		Creature *creature = dynamic_cast<Creature*> (sprite);
		if (creature != NULL) {
			checkCreatureCollision(creature);
		}

		++iter;
		sprite->update(elapsedTime);
	}
	map->update(elapsedTime);
}

void GameManager::checkCreatureCollision(Creature *creature) {
	Sprite *collSprite = getSpriteCollision(creature);
	Fireball *fireball = dynamic_cast<Fireball*> (collSprite);
	if (fireball != NULL) {
		creature->setState(Creature::STATE_DEAD);
		fireball->setExpired(true);
	} else {
		Creature *collCreature = dynamic_cast<Creature*> (collSprite);
		if (collCreature != NULL) {
			Turtle* turtle = dynamic_cast<Turtle*> (creature);
			if (turtle != NULL && turtle->getState()
					== Turtle::STATE_MOVING_SHELL) {
				collCreature->setState(Creature::STATE_DEAD);

			} else {
				creature->setVelocityX(-creature->getVelocityX());
				if (creature->getX() < collCreature->getX()) {
					creature->setX(collCreature->getX() - creature->getWidth());
				} else {
					collCreature->setX(creature->getX()
							- collCreature->getWidth());
				}
				collCreature->setVelocityX(-collCreature->getVelocityX());
			}
		}
	}
}

void GameManager::updateMovableSprite(Sprite *sprite, int elapsedTime) {

	Player *player = dynamic_cast<Player*> (sprite);
	if (player != NULL) {
		if (player->getState() == Player::STATE_ENTERING) {
			player->updatePosition(elapsedTime);
			return;
		} else if (player->getState() == Player::STATE_ENTERED) {
			if (entranceType == ENTRANCE_NEXT_LEVEL) {
				if (isSubmap == false) {
					delete map;
					map = resourceManager.loadNextMap();
					mapChanged = true;
					resetButtons();
				} else {
					delete oldMap->getPlayer();
					Player *newPlayer = (Player*) (map->getPlayer()->clone());
					oldMap->setPlayer(newPlayer);
					int x = pipeEntry->getExitX() * TILE_SIZE + TILE_SIZE / 2;
					int y = pipeEntry->getExitY() * TILE_SIZE;
					newPlayer->setX((float) x);
					newPlayer->setY((float) y);
					newPlayer->setState(Player::STATE_EXITING);
					delete map;
					map = oldMap;
					isSubmap = false;
					map->removePipeEntry(pipeEntry);
				}
				mapChanged = true;
			} else if (entranceType == ENTRANCE_SUBMAP) {
				oldMap = map;
				map = resourceManager.loadMap(*pipeEntry->getSubmapName());
				isSubmap = true;
			}
			return;
		} else if (player->getState() == Player::STATE_EXITING) {
			player->updatePosition(elapsedTime);
			resetButtons();
			return;
		}

	}

	if (!sprite->isFlying()) {
		sprite->setVelocityY(sprite->getVelocityY() + GRAVITY * elapsedTime);
	}

	float dx = sprite->getVelocityX();
	float oldX = sprite->getX();
	float newX = oldX + dx * elapsedTime;
	CollisionTiles *collTiles = getTileCollision(sprite, newX, sprite->getY());
	if (collTiles->getSize() == 0) {
		sprite->setX(newX);
	} else {
		if (dx > 0) {
			sprite->setX(TileMapRenderer::tilesToPixels(
					collTiles->getTile(0)->getX()) - sprite->getWidth());
			if (player != NULL) {
				//cout << "checkingPl"
				checkPipeEntranceCollision(player, collTiles, RIGHT);
				if (player->getState() == Player::STATE_ENTERING) {
					return;
				}
			}
		} else if (dx < 0) {
			sprite->setX(TileMapRenderer::tilesToPixels(
					collTiles->getTile(0)->getX() + 1));
		}
		sprite->collideHorizontal();
	}

	if (player != NULL) {
		int side = RIGHT;
		if (oldX > player->getX())
			side = LEFT;
		checkPlayerCollision(player, side);
		if (mapChanged) {
			return;
		}
	}

	float dy = sprite->getVelocityY();
	float oldY = sprite->getY();
	float newY = oldY + dy * elapsedTime;
	collTiles = getTileCollision(sprite, sprite->getX(), newY);
	if (collTiles->getSize() == 0) {
		sprite->setY(newY);
	} else {
		sprite->collideVertical();
		if (collTiles->getTile(0)->getY() > map->getHeight()) {
			if (player != NULL)
				player->setState(Player::STATE_DYING);
			else
				sprite->setExpired(true);
		} else if (dy > 0) {
			sprite->setY(TileMapRenderer::tilesToPixels(
					collTiles->getTile(0)->getY()) - sprite->getHeight());
			if (player != NULL) {
				checkPipeEntranceCollision(player, collTiles, BOTTOM);
				if (player->getState() == Player::STATE_ENTERING) {
					return;
				}
			}
		} else if (dy < 0) {
			sprite->setY(TileMapRenderer::tilesToPixels(
					collTiles->getTile(0)->getY() + 1));

			if (player != NULL) {
				checkSpecialTileCollision(player, collTiles);
			}
		}

	}
	if (player != NULL) {
		int side = TOP;
		if (oldY > player->getY())
			side = BOTTOM;

		checkPlayerCollision(player, side);
		if (mapChanged) {
			return;
		}
	}
	//	else {
	//		Creature *creature = dynamic_cast<Creature*> (sprite);
	//		if (creature != NULL) {
	//			Fireball *fireball = dynamic_cast<Fireball*> (getSpriteCollision(
	//					creature));
	//			if (fireball != NULL) {
	//				creature->setState(Creature::STATE_DEAD);
	//				fireball->setExpired(true);
	//			}
	//		}
	//	}


}

void GameManager::checkPipeEntranceCollision(Player *player,
		CollisionTiles* collTiles, int side) {
	if (!(side == BOTTOM && moveDown.isPressed() == true) && !(side == RIGHT
			&& moveRight.isPressed() == true))
		return;

	for (int i = 0; i < collTiles->getSize(); i++) {

		PipeEntranceTile *pipeTile =
				dynamic_cast<PipeEntranceTile*> (collTiles->getTile(i));
		if (pipeTile == NULL)
			continue;

		bool canEnter = pipeTile->canEnter(Math::round(player->getX()),
				Math::round(player->getY()), player->getWidth(),
				player->getHeight());
		if (canEnter == false)
			continue;

		NextLevelEntry *nextLevel = map->getNextLevelEntry();
		if (nextLevel != NULL) {
			bool isSame = PipeEntranceTile::isSamePipe(pipeTile->getX(),
					pipeTile->getY(), nextLevel->getX(), nextLevel->getY(),
					side, nextLevel->getSide());
			if (isSame) {
				entranceType = ENTRANCE_NEXT_LEVEL;
				player->setEntranceSide(side);
				player->setState(Player::STATE_ENTERING);
				return;
			}
		}

		PipeEntryList *entryList = map->getPipeEntryList();
		if (entryList == NULL)
			continue;

		for (PipeEntryListIter iter = entryList->begin(); iter
				!= entryList->end(); iter++) {
			bool isSame = PipeEntranceTile::isSamePipe(pipeTile->getX(),
					pipeTile->getY(), (*iter)->getEntranceX(),
					(*iter)->getEntranceY(), side, (*iter)->getEntranceSide());
			if (isSame) {
				pipeEntry = (*iter);
				entranceType = ENTRANCE_SUBMAP;
				player->setEntranceSide(side);
				player->setState(Player::STATE_ENTERING);
				return;
			}
		}
	}
}

void GameManager::checkSpecialTileCollision(Player *player,
		CollisionTiles* collTiles) {

	for (int i = 0; i < collTiles->getSize(); i++) {
		Tile *tile = collTiles->getTile(i);

		QuestionMarkTile *qTile = dynamic_cast<QuestionMarkTile*> (tile);
		if (qTile != NULL) {
			if (qTile->getQuantity() <= 0)
				continue;
			qTile->decreaseQuantity();
			int id = qTile->getSpriteID();
			Sprite* sprite = NULL;

			if (id == ResourceManager::SPRITE_MASHROOM) {
				if (player->getState() == Player::STATE_NORMAL)
					sprite = resourceManager.getAdditionalSprite(id);
				else if (player->getState() == Player::STATE_FIERY) {
					sprite = resourceManager.getAdditionalSprite(
							ResourceManager::SPRITE_STAR);
					qTile->setSpriteID(ResourceManager::SPRITE_STAR);
					qTile->setQuantity(9);
				} else
					sprite = resourceManager.getAdditionalSprite(
							ResourceManager::SPRITE_FLOWER);
			} else {
				sprite = resourceManager.getAdditionalSprite(id);
			}

			sprite->setX((float) TILE_SIZE * tile->getX());
			sprite->setY((float) TILE_SIZE * tile->getY() - TILE_SIZE);
			map->addSprite(sprite);

			BoxStar* boxStar = dynamic_cast<BoxStar*> (sprite);
			if (boxStar != NULL)
				map->getPlayer()->increaseCoins();
			else
				player->setVelocityY(-0.2);

			continue;
		}

		if (player->getState() <= Player::STATE_NORMAL)
			continue;
		BricksTile *bTile = dynamic_cast<BricksTile*> (tile);
		if (bTile != NULL) {
			bTile->setDestroyed(true);
			player->setVelocityY(-0.1);
			BrokenBricks* bricks = resourceManager.getBrokenBricks();
			bricks->setLocation(bTile->getX() * TILE_SIZE, bTile->getY()
					* TILE_SIZE);
			map->addBrokenBricks(bricks);
		}
	}
}

void GameManager::checkPlayerCollision(Player *player, int side) {
	if (player->isAlive() == false) {
		return;
	}

	Sprite *collisionSprite = getSpriteCollision(player);
	if (collisionSprite == NULL)
		return;

	Goodie* goodie = dynamic_cast<Goodie*> (collisionSprite);
	if (goodie != NULL) {

		acquireGoodie(goodie);
		return;
	}

	Creature* creature = dynamic_cast<Creature*> (collisionSprite);

	if (creature != NULL) {
		//bool isNotStateHurt = (player->getState() != Player::STATE_HURT);
		//bool isPlayerDamaged = (creature->getPlayerCollisionReaction(side)
		//		== Creature::REACTION_PLAYER_HURT);

		if (player->getState() != Player::STATE_HURT) {
			creature->handleCollision(player, side);
			//			player->setState(Player::STATE_HURT);
			//		} else if (isNotStateHurt) {
			//			//creature->hurt();
			//			switch (side) {
			//			case TOP:
			//				player->setY(creature->getY() - player->getHeight());
			//				player->jump(true);
			//				break;
			//			case RIGHT:
			//				player->setX(creature->getX() - player->getWidth());
			//				break;
			//			case LEFT:
			//				player->setX(creature->getX() + creature->getWidth());
			//				break;
			//			}
		}

		//		Plant* plant = dynamic_cast<Plant*> (creature);
		//		if (plant != NULL && isNotStateHurt) {
		//			player->setState(Player::STATE_HURT);
		//			return;
		//		}
		//
		//		bool canKill = (side == TOP) && isNotStateHurt ? true : false;
		//		if (canKill) {
		//			creature->setState(Creature::STATE_DYING);
		//			player->setY(creature->getY() - player->getHeight());
		//			player->jump(true);
		//		} else {
		//			player->setState(Player::STATE_HURT);
		//		}
	}
}

void GameManager::acquireGoodie(Goodie *goodie) {

	Star *star = dynamic_cast<Star*> (goodie);
	if (star != NULL) {
		map->removeSprite(goodie);
		map->getPlayer()->increaseCoins();
		return;
	}

	Mashroom *mashroom = dynamic_cast<Mashroom*> (goodie);
	if (mashroom != NULL) {
		map->removeSprite(goodie);
		map->getPlayer()->setState(Player::STATE_SUPER);
		return;
	}

	Flower *flower = dynamic_cast<Flower*> (goodie);
	if (flower != NULL) {
		map->removeSprite(goodie);
		map->getPlayer()->setState(Player::STATE_FIERY);
		return;
	}
}

void GameManager::resetButtons() {
	shoot.reset();
	moveLeft.reset();
	moveRight.reset();
	jump.reset();
}
