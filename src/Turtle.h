/*
 * Turtle.h
 *
 *  Created on: 2009-12-11
 *      Author: Revers
 */

#ifndef TURTLE_H_
#define TURTLE_H_

#include "Creature.h"

class Turtle: public Creature {
	Animation *flyingLeft;
	Animation *flyingRight;

	SDL_Surface *shellOne;
	SDL_Surface *shellTwo;
	int playerCollSide;
	void hurt();
public:
	static const int STATE_SHELL = 3;
	static const int STATE_MOVING_SHELL = 4;
	static const int STATE_FLYING = 5;

	Turtle(Animation *left, Animation *right, SDL_Surface *shellOne,
			SDL_Surface *shellTwo);
	Turtle(Animation *left, Animation *right, Animation *flyingLeft,
			Animation *flyingRight, SDL_Surface *shellOne,
			SDL_Surface *shellTwo);
	virtual ~Turtle();

	virtual void update(int elapsedTime);
	virtual void cleanHost();
	virtual void handleCollision(Player *player, int side);
	virtual void collideVertical();

	virtual Sprite* clone();
	virtual inline float getMaxSpeed();
	//virtual int getPlayerCollisionReaction(int side);


	virtual void setState(int state);

	//	virtual inline int getWidth();
	//	virtual inline int getHeight();

};

inline float Turtle::getMaxSpeed() {
	return 0.1;
}



//inline int Turtle::getHeight() {
//	return 61;
//}
//inline int Turtle::getWidth() {
//	return 40;
//}
#endif /* TURTLE_H_ */
