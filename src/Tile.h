/*
 * Tile.h
 *
 *  Created on: 2009-12-05
 *      Author: Revers
 */

#ifndef TILE_H_
#define TILE_H_
#include "SDL/SDL.h"

class Tile {
protected:
	int x;
	int y;
	int width;
	int height;
	SDL_Surface *image;
	bool visible;
	bool destroyed;
public:
	Tile();
	Tile(SDL_Surface *image);

	virtual void update(int elapsedTime) {
	}

	virtual Tile* clone();
	virtual void cleanHost();
	virtual ~Tile();

	inline int getWidth();
	inline int getHeight();
	inline void setWidth(int width);
	inline void setHeight(int height);
	inline SDL_Surface* getImage();
	inline bool isVisible();
	inline void setVisible(bool visible);

	inline void setX(int x);
	inline int getX();
	inline void setY(int y);
	inline int getY();

	inline void setDestroyed(bool destroyed);
	inline bool isDestroyed();
};

inline void Tile::setDestroyed(bool destroyed) {
	this->destroyed = destroyed;
}

inline bool Tile::isDestroyed() {
	return destroyed;
}

inline void Tile::setX(int x) {
	this->x = x;
}

inline int Tile::getX() {
	return x;
}

inline void Tile::setY(int y) {
	this->y = y;
}

inline int Tile::getY() {
	return y;
}
inline bool Tile::isVisible() {
	return visible;
}

inline void Tile::setVisible(bool visible) {
	this->visible = visible;
}

inline void Tile::setWidth(int width) {
	this->width = width;
}

inline void Tile::setHeight(int height) {
	this->height = height;
}

inline SDL_Surface* Tile::getImage() {
	return image;
}

inline int Tile::getWidth() {
	return width;
}

inline int Tile::getHeight() {
	return height;
}

#endif /* TILE_H_ */
