/*
 * PipeEntranceTile.h
 *
 *  Created on: 2009-12-09
 *      Author: Revers
 */

#ifndef PIPEENTRANCETILE_H_
#define PIPEENTRANCETILE_H_
#include "Tile.h"

class PipeEntranceTile: public Tile {
	int side;
public:
//	static const int LEFT = 0;
//	static const int RIGHT = 1;
//	static const int DOWN = 3;
//	static const int UP = 4;

	PipeEntranceTile(SDL_Surface* image, int side);
	virtual ~PipeEntranceTile();

	virtual Tile* clone();

	bool canEnter(int x, int y, int width, int height);
	static bool isSamePipe(int pipe1X, int pipe1Y, int pipe2X, int pipe2Y,
			int pipe1Side, int pipe2Side);

};

#endif /* PIPEENTRANCETILE_H_ */
