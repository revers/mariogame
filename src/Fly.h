/*
 * Fly.h
 *
 *  Created on: 2009-11-29
 *      Author: Revers
 */

#ifndef FLY_H_
#define FLY_H_
#include "Creature.h"

class Fly: public Creature {
public:
	inline Fly(Animation *left, Animation *right, SDL_Surface* deadImage);
	virtual float getMaxSpeed();
	virtual bool isFlying() {
		return isAlive();
	}
	inline Sprite* clone();
	virtual inline ~Fly();
};

inline Fly::Fly(Animation *left, Animation *right, SDL_Surface* deadImage) :
	Creature(left, right, deadImage) {
}

inline Fly::~Fly() {
	delete leftWalking;
	delete rightWalking;
}

inline Sprite* Fly::clone() {
	//std::cout << "fly.clone()" << std::endl;
	return new Fly(leftWalking->clone(), rightWalking->clone(), deadImage);
}

float Fly::getMaxSpeed() {
	return 0.2f;
}

//inline bool Fly::isFlying() {
//	return isAlive();
//}

#endif /* FLY_H_ */
