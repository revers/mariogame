/*
 * QuestionMarkTile.cpp
 *
 *  Created on: 2009-12-06
 *      Author: Revers
 */

#include "QuestionMarkTile.h"

QuestionMarkTile::QuestionMarkTile(Animation *anim, SDL_Surface* emptyBox,
		int spriteID, int quantity) :
	Tile(anim->getImage()) {
	this->anim = anim;
	this->emptyBox = emptyBox;
	this->spriteID = spriteID;
	this->quantity = quantity;
}

QuestionMarkTile::~QuestionMarkTile() {
	delete anim;
}

void QuestionMarkTile::update(int elapsedTime) {
	if (quantity == 0)
		image = emptyBox;
	else {
		anim->update(elapsedTime);
		image = anim->getImage();
	}
}

Tile* QuestionMarkTile::clone() {
	return new QuestionMarkTile(anim->clone(), emptyBox, spriteID, quantity);
}

void QuestionMarkTile::cleanHost() {
	anim->cleanHost();
	SDL_FreeSurface(emptyBox);
}
